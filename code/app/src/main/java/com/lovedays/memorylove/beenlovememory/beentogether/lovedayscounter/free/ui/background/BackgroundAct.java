package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background.adapter.BackgroundAdapter;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background.adapter.BackgroundListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.BackgroundActBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.take_image.TakeImageAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.FileUtils;

public class BackgroundAct extends AbsActivity<BackgroundActBinding, BackgroundViewModel> {

    private static final int REQUEST_BACKGROUND = 100;

    public static Intent getIntent(@NonNull Context context) {
        return new Intent(context, BackgroundAct.class);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.background_act;
    }

    @Override
    protected Class<BackgroundViewModel> getViewModelClass() {
        return BackgroundViewModel.class;
    }

    private BackgroundAdapter adapter;

    private BackgroundListener backgroundListener = new BackgroundListener() {
        @Override
        public void onSelectBackground(String path) {
            viewModel.saveBackground(path);
            initResultOk();
        }

        @Override
        public void onSelectImageFromGallery() {
            navigateSelectImageBackground();
        }
    };

    private void initResultOk() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void initView() {

        initToolBar();
        initAdapter();
    }

    private void initToolBar() {
        setSupportActionBar(binding.toolBar);
    }


    private void initAdapter() {
        adapter = new BackgroundAdapter(viewModel.getListBackground(), backgroundListener);
        binding.listBackground.setLayoutManager(new GridLayoutManager(this, 2));
        binding.listBackground.setAdapter(adapter);
    }

    private void navigateSelectImageBackground() {
        FileUtils.externalPermision(this, new FileUtils.PerListener() {
            @Override
            public void onGranted() {
                startActivityForResult(TakeImageAct.getIntentFull(BackgroundAct.this),
                        REQUEST_BACKGROUND);
            }

            @Override
            public void onDeny() {
                Toast.makeText(BackgroundAct.this,getString(R.string.permission_on_deny), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == REQUEST_BACKGROUND) {

                Uri uri = data.getParcelableExtra(TakeImageAct.RESULT_IMAGE);
                if (uri != null) {
                    viewModel.saveBackground(uri.toString());
                    initResultOk();
                }
            }
        }
    }
}
