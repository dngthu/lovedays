package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter;

import android.text.style.UpdateLayout;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.AdapterListUpdateCallback;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListUpdateCallback;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

import java.util.ArrayList;
import java.util.List;

public class DiaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DiaryTable> list = new ArrayList<>();

    @NonNull
    private DiaryListener diaryListener;
    private int HEADER_COUNT = 1;

    public DiaryAdapter(@NonNull DiaryListener diaryListener) {
        this.diaryListener = diaryListener;

    }

    public void submitList(List<DiaryTable> newList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiaryDiffCallback(newList, list));
        diffResult.dispatchUpdatesTo(new ListUpdateCallback() {
            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position + HEADER_COUNT, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position + HEADER_COUNT, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition + HEADER_COUNT, toPosition+ HEADER_COUNT);
            }

            @Override
            public void onChanged(int position, int count, @Nullable Object payload) {
                notifyItemRangeChanged(position + HEADER_COUNT, count, payload);
            }
        });
        list.clear();
        this.list.addAll(newList);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == R.layout.diary_header_item) {
            return DiaryHeaderViewHolder.create(parent, diaryListener);
        }
        return DiaryViewHolder.create(parent, diaryListener);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return R.layout.diary_header_item;
        return R.layout.diary_item;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DiaryViewHolder) {
            if (position > 0 && position <= list.size()) {
                ((DiaryViewHolder) holder).bind(list.get(position - HEADER_COUNT));
            }
        } else if (holder instanceof DiaryHeaderViewHolder) {
            ((DiaryHeaderViewHolder) holder).bind();
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + HEADER_COUNT;
    }
}
