package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

import java.util.List;

public class DiaryDiffCallback extends DiffUtil.Callback  {

    List<DiaryTable> newList;
    List<DiaryTable> oldList;


    public DiaryDiffCallback(@NonNull List<DiaryTable> newList,
                             @NonNull List<DiaryTable> list) {
        this.newList = newList;
        this.oldList = list;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        DiaryTable old = oldList.get(oldItemPosition);
        DiaryTable newM = newList.get(newItemPosition);
        return TextUtils.equals(old.getId(), newM.getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        DiaryTable old = oldList.get(oldItemPosition);
        DiaryTable newM = newList.get(newItemPosition);
        return TextUtils.equals(old.getId(), newM.getId())
                && TextUtils.equals(old.getTitle(), newM.getTitle())
                && TextUtils.equals(old.getCover(), newM.getCover())
                && TextUtils.equals(old.getDesciption(), newM.getDesciption())
                && old.getTimeCreate() == newM.getTimeCreate();
    }
}
