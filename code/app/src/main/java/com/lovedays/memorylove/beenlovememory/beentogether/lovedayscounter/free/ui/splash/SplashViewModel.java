package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.splash;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

public class SplashViewModel  extends AbsViewModel {
    public String getCurrentBackground() {
        return getManager().getCurrentBackground();
    }
}
