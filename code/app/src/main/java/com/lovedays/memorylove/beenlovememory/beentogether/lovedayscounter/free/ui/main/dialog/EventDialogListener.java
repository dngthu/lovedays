package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog;

public interface EventDialogListener {

    void onEditEvent();
    void onDeleteEvent();
}
