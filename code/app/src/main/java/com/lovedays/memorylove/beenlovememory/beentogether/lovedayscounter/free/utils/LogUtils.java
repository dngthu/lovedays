package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;

public class LogUtils {

    public static void log(String content) {
        Log.d("TESTAPP", content);
    }

    public static void log(String content, Object object) {
        if (object == null) {
            log(content + " NULL");
        } else if (object instanceof String) {
            log(content + " " + object);
        } else {
            log(content + " " + new Gson().toJson(object));
        }
    }
}
