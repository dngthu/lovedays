package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

public class EventModel implements Parcelable {
    private String id;
    private String title;
    private long time;
    private boolean annual;


    public EventModel(String title, long time, boolean annual) {
        this.id = UUID.randomUUID().toString();
        this.title = title;
        this.annual = annual;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isAnnual() {
        return annual;
    }

    public void setAnnual(boolean annual) {
        this.annual = annual;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeLong(this.time);
        dest.writeByte(this.annual ? (byte) 1 : (byte) 0);
    }

    protected EventModel(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.time = in.readLong();
        this.annual = in.readByte() != 0;
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel source) {
            return new EventModel(source);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };
}
