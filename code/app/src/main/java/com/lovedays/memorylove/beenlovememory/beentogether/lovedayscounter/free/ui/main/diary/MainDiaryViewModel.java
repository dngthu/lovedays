package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary;

import androidx.lifecycle.LiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

import java.util.List;

public class MainDiaryViewModel extends AbsViewModel {

    LiveData<List<DiaryTable>> listDiary;

    public MainDiaryViewModel() {
        listDiary = getDiary().getListDiary();
    }
}
