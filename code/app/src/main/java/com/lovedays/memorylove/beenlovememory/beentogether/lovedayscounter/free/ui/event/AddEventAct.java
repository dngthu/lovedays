package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.event;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.view.View;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;

import com.bumptech.glide.Glide;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.AddEventActBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.EventModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;

import java.util.Calendar;

public class AddEventAct extends AbsActivity<AddEventActBinding, AddEventViewModel> {

    private static final String EDIT_EVENT_MODEL = "AddEventAct-EDIT_EVENT_MODEL";


    public static Intent getIntent(@NonNull Context context) {
        return new Intent(context, AddEventAct.class);
    }

    public static Intent getIntent(@NonNull Context context, String id) {
        Intent intent = new Intent(context, AddEventAct.class);
        intent.putExtra(EDIT_EVENT_MODEL, id);
        return intent;
    }

    @Override
    protected boolean isTransparentStatusBar() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.add_event_act;
    }

    @Override
    protected Class<AddEventViewModel> getViewModelClass() {
        return AddEventViewModel.class;
    }

    private DatePickerDialog datePickerDialog;

    private DatePickerDialog.OnDateSetListener datePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    viewModel.startDate.setValue(DateUtils.formatDay(myCalendar));
                }
            };

    @Override
    protected void initView() {
        initIntent();
        initBackground();
        initDate();
        initHandleEditModel();
        initHandleStartTime();
        bindAction();
    }

    private void initHandleEditModel() {
        viewModel.editModel.observe(this, new Observer<EventModel>() {
            @Override
            public void onChanged(EventModel eventModel) {
                if (eventModel != null) {
                    initDate(eventModel.getTime());
                    binding.editTitle.setText(eventModel.getTitle());
                    binding.annualCb.setChecked(eventModel.isAnnual());
                }
            }
        });
    }

    private void initIntent() {
        if (getIntent() != null) {
            String id = getIntent().getStringExtra(EDIT_EVENT_MODEL);
            EventModel model = viewModel.getEventModel(id);
            if (model != null) {
                viewModel.editModel.setValue(model);
            }
        }
    }

    private void bindAction() {
        binding.dateEventTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar myCalender = Calendar.getInstance();
                Long time = viewModel.startDate.getValue();
                myCalender.setTimeInMillis(time != null ? time : System.currentTimeMillis());
                showDialogDatePicker(myCalender);
            }
        });

        binding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Editable edt = binding.editTitle.getText();
                String str = edt == null ? "" : edt.toString().trim();
                if (str.isEmpty()) {
                    binding.editTitle.setError(getString(R.string.add_event_input_error));
                } else {
                    initResult(str);
                }
            }
        });
    }

    private void initResult(@NonNull String str) {
        EventModel eventModel = viewModel.editModel.getValue();
        Long time = viewModel.startDate.getValue();
        long timeLong = time == null ? DateUtils.toDay() : time;
        boolean annual = binding.annualCb.isChecked();


        if (eventModel == null) {
            //Th tao moi eventModel
            eventModel = new EventModel(str, timeLong, annual);
        } else {
            //Th edit , update infor
            eventModel.setTime(timeLong);
            eventModel.setTitle(str);
            eventModel.setAnnual(annual);
        }

        Intent intent = new Intent();
        intent.putExtra(EventModel.class.getName(), eventModel);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void initHandleStartTime() {
        viewModel.startDate.observe(this, new Observer<Long>() {
            @Override
            public void onChanged(Long aLong) {
                if (aLong != null) {
                    String dateStr = DateUtils.getDateString(aLong);
                    binding.dateEventTv.setText(dateStr);
                }
            }
        });
    }

    private void initDate(long time) {
        viewModel.startDate.setValue(time);
    }

    private void initDate() {
        viewModel.startDate.setValue(System.currentTimeMillis());
    }

    private void initBackground() {
        Glide.with(this)
                .load(viewModel.getCurrentBackground())
                .into(binding.imageBg);
    }

    private void showDialogDatePicker(Calendar myCalendar) {
        if (datePickerDialog == null) {
            datePickerDialog = new DatePickerDialog(this,
                    datePickerListener,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            Calendar maxCalendar = Calendar.getInstance();
            Calendar minCalendar = Calendar.getInstance();
            minCalendar.add(Calendar.YEAR, -100);
            maxCalendar.add(Calendar.YEAR, 100);

            datePickerDialog.getDatePicker().setMaxDate(maxCalendar.getTimeInMillis());
            datePickerDialog.getDatePicker().setMinDate(minCalendar.getTimeInMillis());
        }
        datePickerDialog.show();
    }
}
