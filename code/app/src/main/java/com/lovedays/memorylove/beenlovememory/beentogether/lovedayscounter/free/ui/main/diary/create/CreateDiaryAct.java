package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.Const;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.CreateDiaryActBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.model.DiaryEntity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentType;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view.CreateDiaryImageView;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view.CreateDiaryListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view.CreateDiaryTextView;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.DetailDiaryAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.take_image.TakeImageAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class CreateDiaryAct extends AbsActivity<CreateDiaryActBinding, CreateDiaryViewModel> implements CreateDiaryListener {


    private static final int REQUEST_SELECT_IMAGE = 111;
    private static final int REQUEST_SELECT_COVER = 112;
    private static final String DIARY_ID = "CreateDiaryAct-DIARY_ID";

    public static Intent getIntent(@NonNull Context context) {
        return new Intent(context, CreateDiaryAct.class);
    }

    public static Intent getIntentEdit(@NonNull Context context, @NonNull String id) {
        Intent intent = new Intent(context, CreateDiaryAct.class);
        intent.putExtra(DIARY_ID, id);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.create_diary_act;
    }

    @Override
    protected Class<CreateDiaryViewModel> getViewModelClass() {
        return CreateDiaryViewModel.class;
    }

    @Override
    protected void initView() {
        setSupportActionBar(binding.toolBar);
        initIntent();
        initCreateDiaryView();
        initHandleDiaryEdit();
        initHandleDiaryCreated();
        bindActions();
    }


    private void initCreateDiaryView() {
        CreateDiaryTextView view = new CreateDiaryTextView(CreateDiaryAct.this);
        view.setCreateDiaryListener(CreateDiaryAct.this);
        binding.listItem.addView(view);
    }

    private void initEditDiaryView(@NonNull List<DiaryContentTable> contentTables) {

        binding.listItem.removeAllViews();
        for (DiaryContentTable table : contentTables) {
            if (table.getType() == DiaryContentType.TEXT) {
                CreateDiaryTextView view = new CreateDiaryTextView(CreateDiaryAct.this);
                view.setCreateDiaryListener(CreateDiaryAct.this);
                view.setModel(table);
                binding.listItem.addView(view);
            } else if (table.getType() == DiaryContentType.IMAGE) {
                CreateDiaryImageView view = new CreateDiaryImageView(CreateDiaryAct.this);
                view.setCreateDiaryListener(CreateDiaryAct.this);
                view.updateModel(table);
                binding.listItem.addView(view);
            }
        }
    }

    private void initIntent() {
        String id = getIntent().getStringExtra(DIARY_ID);
        if (id != null && !id.isEmpty()) {
            viewModel.diaryEditId.setValue(id);
        }else {
            //set cover default
            binding.header.setCoverUrl(Const.DIARY_COVER_DEFAULT);
        }
    }

    private void initHandleDiaryCreated() {

        viewModel.diaryTableCreated.observe(this, diaryTable -> {
            if (diaryTable!= null){
                if (!viewModel.isEditDiary()){
                    startActivity(DetailDiaryAct.getIntent(CreateDiaryAct.this, diaryTable.getId()));
                }
                finish();
            }
        });
    }


    private void initHandleDiaryEdit() {
        viewModel.diaryTableEdit.observe(this, diaryTable -> {
            if (diaryTable != null) {
                binding.header.setModel(diaryTable);
            }
        });

        viewModel.contentTables.observe(this, contentTables -> {
            if (contentTables != null && !contentTables.isEmpty()) {
                initEditDiaryView(contentTables);
            }
        });
    }

    private void bindActions() {
        binding.header.setCreateDiaryListener(this);
        binding.footer.setListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            Uri uri;
            switch (requestCode) {
                case REQUEST_SELECT_COVER:
                    uri = data.getParcelableExtra(TakeImageAct.RESULT_IMAGE);
                    if (uri != null) {
                        binding.header.setCoverUrl(uri.toString());
                    }
                    break;
                case REQUEST_SELECT_IMAGE:
                    uri = data.getParcelableExtra(TakeImageAct.RESULT_IMAGE);
                    if (uri != null) {
                        CreateDiaryImageView view = new CreateDiaryImageView(CreateDiaryAct.this);
                        view.setCreateDiaryListener(this);
                        view.updateImage(uri);
                        binding.listItem.addView(view);
                    }
                    break;

            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_diary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save_action) {
            onCreateDiary();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onCreateDiary() {
        DiaryTable model = binding.header.getModel();
        if (model != null) {
            int count = binding.listItem.getChildCount();
            List<DiaryContentTable> modelContents = new ArrayList<>();
            for (int i = 0; i < count; i++) {
                View view = binding.listItem.getChildAt(i);
                DiaryContentTable contentModel = null;
                if (view instanceof CreateDiaryImageView) {
                    contentModel = ((CreateDiaryImageView) view).getContent();
                } else if (view instanceof CreateDiaryTextView) {
                    contentModel = ((CreateDiaryTextView) view).getContent();
                }
                if (contentModel != null) {
                    contentModel.setDiaryId(model.getId());
                    modelContents.add(contentModel);
                }
            }
            viewModel.onCreateDiary(new DiaryEntity(model, modelContents));
            Toast.makeText(this, "Insert Complete", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Ban chua dat tieu de", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public LifecycleOwner getLifecycleOwner() {
        return this;
    }

    @Override
    public void onRemoveText(@NonNull CreateDiaryTextView createDiaryTextView) {
        DiaryContentTable model = createDiaryTextView.getContent();
        if (model != null) {
            new AlertDialog.Builder(this)
                    .setTitle("Delete entry")
                    .setMessage("Are you sure you want to delete this entry?")
                    .setPositiveButton(android.R.string.yes, (dialog, which) ->
                            binding.listItem.removeView(createDiaryTextView))
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            binding.listItem.removeView(createDiaryTextView);
        }
    }

    @Override
    public void onRemoveImage(@NonNull CreateDiaryImageView createDiaryImageView) {
        DiaryContentTable model = createDiaryImageView.getContent();
        if (model != null) {
            new AlertDialog.Builder(this)
                    .setTitle("Delete entry")
                    .setMessage("Are you sure you want to delete this entry?")
                    .setPositiveButton(android.R.string.yes, (dialog, which) ->
                            binding.listItem.removeView(createDiaryImageView))
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            binding.listItem.removeView(createDiaryImageView);
        }
    }

    @Override
    public void addText() {
        CreateDiaryTextView view = new CreateDiaryTextView(CreateDiaryAct.this);
        view.setCreateDiaryListener(CreateDiaryAct.this);
        binding.listItem.addView(view);
    }

    @Override
    public void addImage() {
        FileUtils.externalPermision(CreateDiaryAct.this, new FileUtils.PerListener() {
            @Override
            public void onGranted() {
                startActivityForResult(TakeImageAct.getIntentRatio(CreateDiaryAct.this, 4f/3), REQUEST_SELECT_IMAGE);
            }

            @Override
            public void onDeny() {
                Toast.makeText(CreateDiaryAct.this, getString(R.string.permission_on_deny), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onLoadCover() {
        FileUtils.externalPermision(CreateDiaryAct.this, new FileUtils.PerListener() {
            @Override
            public void onGranted() {
                startActivityForResult(TakeImageAct.getIntentRatio(CreateDiaryAct.this, 1.5f), REQUEST_SELECT_COVER);
            }

            @Override
            public void onDeny() {
                Toast.makeText(CreateDiaryAct.this, getString(R.string.permission_on_deny), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
