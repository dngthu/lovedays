package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create;

import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.model.DiaryEntity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

import java.util.List;

public class CreateDiaryViewModel extends AbsViewModel {

    MutableLiveData<String> diaryCreateId = new MutableLiveData<>();
    LiveData<DiaryTable> diaryTableCreated ;

    MutableLiveData<String> diaryEditId = new MutableLiveData<>();

    LiveData<DiaryTable> diaryTableEdit ;

    LiveData<List<DiaryContentTable>> contentTables ;

    public CreateDiaryViewModel() {

        diaryTableCreated = Transformations.switchMap(diaryCreateId, input -> {
            if (input!=null && !input.isEmpty()){
                return getDiary().getDiaryTable(input);
            }
            return new MutableLiveData<>();
        });


        diaryTableEdit = Transformations.switchMap(diaryEditId, input -> {
            if (input!=null && !input.isEmpty()){
                return getDiary().getDiaryTable(input);
            }
            return new MutableLiveData<>();
        });

        contentTables = Transformations.switchMap(diaryEditId, input -> {
            if (input!=null && !input.isEmpty()){
                return getDiary().getListDiaryContent(input);
            }
            return new MutableLiveData<>();
        });
    }

    public void onCreateDiary(DiaryEntity diaryEntity) {
        diaryCreateId.setValue(diaryEntity.getDiaryTable().getId());
        getDiary().insertDiaryEntity(diaryEntity);
    }

    public boolean isEditDiary() {
        String editId = diaryEditId.getValue();
        return editId!=null && !editId.isEmpty() && TextUtils.equals(editId, diaryCreateId.getValue());
    }
}
