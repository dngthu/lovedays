package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.UserLoveDialogBinding;

public class UserLoveDialog extends BottomSheetDialog {
    private Listener listener;
    private UserLoveDialogBinding binding;

    public UserLoveDialog(@NonNull Context context) {
        super(context);
        initView();
    }

    public UserLoveDialog(@NonNull Context context, int theme) {
        super(context, theme);
        initView();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void initView() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.user_love_dialog,
                null, false);
        setContentView(binding.getRoot());
        bindAction();
    }

    private void bindAction() {
        binding.changeAvatarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null) listener.onChangeAvatar();
                dismiss();
            }
        });
        binding.changeNameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null) listener.onChangeName();
                dismiss();
            }
        });
        binding.removeAvatarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null) listener.onRemoveAvatar();
                dismiss();
            }
        });
    }

    public interface Listener{
        void onChangeName();
        void onChangeAvatar();
        void onRemoveAvatar();
    }
}
