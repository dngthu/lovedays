package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.setting;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

public class SettingViewModel extends AbsViewModel {

    public SettingViewModel() {

        getManager().getCurrentLoveModel();
    }

    public LoveDayModel getLoveDayModel() {
        return getManager().getCurrentLoveModel();
    }

    public boolean isArlertEvent() {
        return getManager().isArlertEvent();
    }

    public boolean isShowInStatusBar() {
        return getManager().isShowInStatusBar();
    }
}
