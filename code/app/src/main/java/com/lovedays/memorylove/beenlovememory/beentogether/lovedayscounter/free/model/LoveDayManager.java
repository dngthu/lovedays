package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.Const;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.cache.Preferences;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class LoveDayManager {

    private static LoveDayManager manager;
    @NonNull
    private final Preferences preferences;

    private LoveDayManager(@NonNull Preferences preferences) {
        this.preferences = preferences;
    }

    public static LoveDayManager getManager(@NonNull Preferences preferences) {
        if (manager == null) manager = new LoveDayManager(preferences);
        return manager;
    }

    public static void reset(){
        manager= null;
    }

    private MutableLiveData<List<TimeModel>> timeModelList = new MutableLiveData<>();
    private TimeModel nextTimeModel;
    private int indextComming;

    public int getIndextComming() {
        return indextComming;
    }

    public TimeModel getNextTimeModel() {
        return nextTimeModel;
    }

    public MutableLiveData<List<TimeModel>> getTimeModelList() {
        List<TimeModel> value = timeModelList.getValue();
        if (value == null || value.isEmpty()) {
            refreshList();
        }
        return timeModelList;
    }

    private void refreshList() {
        sendBroadcastUpdateWidget();
        LoveDayModel curentModel = getCurrentLoveModel();
        if (curentModel != null) {
            timeModelList.setValue(renderListTime(getCurrentLoveModel()));
        }
    }

    private void sendBroadcastUpdateWidget() {
        LoveDayApp.getInstance().sendBroadcastUpdateWidget();
    }

    private void checkIsNextTimeModel(@NonNull TimeModel timeModel) {
        if (timeModel.getCount() <= 0) {
            if (nextTimeModel == null || nextTimeModel.getCount() < timeModel.getCount()) {
                nextTimeModel = timeModel;
            }
        }
    }

    @NonNull
    private List<TimeModel> renderListTime(LoveDayModel loveDayModel) {
        List<TimeModel> listTimes = new ArrayList<>();
        EventModels eventModels = getEventModels();
        nextTimeModel = null;

        if (loveDayModel != null) {
            long startTime = loveDayModel.getStartTime();
            Calendar startCalender = Calendar.getInstance();
            startCalender.setTime(new Date(startTime));
            boolean start0 = loveDayModel.isStartDay0();

            int start = start0 ? 100 : 99;
            for (int i = start; i < 18000; i = i + 100) {
                Calendar timeCa = Calendar.getInstance();
                timeCa.setTimeInMillis(startTime);
                timeCa.add(Calendar.DAY_OF_YEAR, i);
                long time = timeCa.getTimeInMillis();
                int day = start0 ? i : i + 1;
                String title = String.format(
                        LoveDayApp.getInstance().getString(R.string.love_day_render_title_count_day),
                        day);
                String date = DateUtils.getDateString(time);
                int count = DateUtils.getCountDay2(start0, time, DateUtils.toDay());
                TimeModel timeModel = new TimeModel(time, title, date, count);
                checkIsNextTimeModel(timeModel);
                if (loveDayModel.isShow100Day()) {
                    listTimes.add(timeModel);
                }
            }

            for (int i = 1; i <= 100; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(startCalender.get(Calendar.YEAR),
                        startCalender.get(Calendar.MONTH),
                        startCalender.get(Calendar.DAY_OF_MONTH));
                calendar.add(Calendar.YEAR, i);

                long time = calendar.getTimeInMillis();
                String title = String.format(
                        LoveDayApp.getInstance().getString(R.string.love_day_render_title_count_year), i);
                String date = DateUtils.getDateString(time);
                int count = DateUtils.getCountDay2(start0, time, DateUtils.toDay());

                TimeModel timeModel = new TimeModel(time, title, date, count);
                checkIsNextTimeModel(timeModel);

                if (loveDayModel.isShowYear()) {

                    listTimes.add(timeModel);
                }

                if (eventModels != null) {
                    List<EventModel> listAnnual = eventModels.getEventAnnuals(true);
                    for (EventModel eventModel : listAnnual) {

                        Calendar calendar1 = Calendar.getInstance();
                        calendar1.setTimeInMillis(eventModel.getTime());
                        calendar1.set(Calendar.YEAR, calendar.get(Calendar.YEAR));

                        long timeEvent = calendar1.getTimeInMillis();
                        String titleEvent = eventModel.getTitle();
                        String dateEvent = DateUtils.getDateString(timeEvent);

                        int countEvent = DateUtils.getCountDay2(start0, timeEvent, DateUtils.toDay());
                        String whatId = eventModel.getId();
                        listTimes.add(new TimeModel(timeEvent, titleEvent, dateEvent, countEvent, whatId));
                    }
                }
            }

            if (eventModels != null) {
                List<EventModel> listAnnual = eventModels.getEventAnnuals(false);
                for (EventModel eventModel : listAnnual) {

                    long timeEvent = eventModel.getTime();
                    String titleEvent = eventModel.getTitle();
                    String dateEvent = DateUtils.getDateString(timeEvent);
                    int countEvent = DateUtils.getCountDay2(start0, timeEvent, DateUtils.toDay());

                    String whatId = eventModel.getId();
                    listTimes.add(new TimeModel(timeEvent, titleEvent, dateEvent, countEvent, whatId));
                }
            }

            Collections.sort(listTimes, new Comparator<TimeModel>() {
                @Override
                public int compare(TimeModel timeModel, TimeModel t1) {
                    Date date1 = new Date(timeModel.getTime());
                    Date date2 = new Date(t1.getTime());
                    return date1.compareTo(date2);
                }
            });

            int indext = Iterators.indexOf(listTimes.iterator(), new Predicate<TimeModel>() {
                @Override
                public boolean apply(@NullableDecl TimeModel input) {
                    return input != null && input.getCount() <= 0;
                }
            });
            if (indext >= 0) indextComming = indext;
            else indextComming = -1;
            if (!listTimes.isEmpty()
                    && indextComming >= 0
                    && indextComming < listTimes.size()) {
                listTimes.get(indextComming).setComming(true);
            }
        }
        return listTimes;
    }


    //load local data
    public List<String> getListBackground() {
        List<String> result = new ArrayList<>();
        try {
            String[] list = LoveDayApp.getInstance().getAssets().list(Const.BG_ASSETS_FOLDER);
            if (list != null) {
                for (String content : list) {
                    result.add(Const.ASSETS_FOLDER + Const.BG_ASSETS_FOLDER + File.separator + content);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public EventModel getEventModel(String whatId) {
        EventModels models = getEventModels();
        if (models != null) return models.getEventModel(whatId);
        return null;
    }


    //save data
    public void changeTimeLoveModel(long timeInMillis) {
        LoveDayModel curent = getCurrentLoveModel();
        if (curent != null) {
            curent.setStartTime(timeInMillis);
            preferences.putData(Const.START_LOVE_DAY, curent);
        } else {
            initLoveModel(timeInMillis);
        }
        refreshList();
    }

    public void initLoveModel(long longTime) {
        preferences.putData(Const.START_LOVE_DAY, new LoveDayModel(longTime));
    }

    public void updateLoveModel(@NonNull LoveDayModel model) {
        preferences.putData(Const.START_LOVE_DAY, model);
        refreshList();
    }

    public void saveBackground(String path) {
        if (!TextUtils.isEmpty(path)) {
            preferences.putData(Const.BACKGROUND, path);
            sendBroadcastUpdateWidget();
        }
    }

    public void addEventModel(@NonNull EventModel model) {
        EventModels models = getEventModels();
        if (models == null) {
            models = new EventModels();
        }
        models.addEventModel(model);
        preferences.putData(Const.EVENT_MODELS, models);
        refreshList();
    }

    public void removeEvent(String whatId) {
        EventModels models = getEventModels();
        if (models != null) {
            models.removeEvent(whatId);
        }
        preferences.putData(Const.EVENT_MODELS, models);
        refreshList();
    }

    public void saveBottomTitle(String content) {
        preferences.putData(Const.BOTTOM_TITLE, content);
        sendBroadcastUpdateWidget();
    }

    public void saveTopTitle(String content) {
        preferences.putData(Const.TOP_TITLE, content);
        sendBroadcastUpdateWidget();
    }

    public void updateUserLoveModel(UserLoveModel model) {
        preferences.putData(Const.USER_LOVE_MODEL, model);
        sendBroadcastUpdateWidget();
    }

    public void saveArlertEvent(boolean b) {
        preferences.putData(Const.ARLERT_NOTIFCATION, b);
    }

    public void saveShowInStatusBar(boolean b) {
        preferences.putData(Const.SHOW_STATUS_BAR, b);
    }

    public void saveColorBackground(int selectedColor) {
        preferences.putData(Const.COLOR_BACKGROUND, selectedColor);
        sendBroadcastUpdateWidget();
    }

    //load current data
    public LoveDayModel getCurrentLoveModel() {
        return preferences.getObject(Const.START_LOVE_DAY, LoveDayModel.class);
    }

    private EventModels getEventModels() {
        return preferences.getObject(Const.EVENT_MODELS, EventModels.class);
    }

    @NonNull
    public UserLoveModel getUserLoveModel() {
        UserLoveModel model = preferences.getObject(Const.USER_LOVE_MODEL, UserLoveModel.class);
        if (model != null) return model;
        return UserLoveModel.empty();
    }

    public String getCurrentBackground() {
        String current = preferences.getData(Const.BACKGROUND, Const.BACKGROUND_DEFAULT);
        if (!TextUtils.isEmpty(current)) {
            return current;
        }
        return Const.BACKGROUND_DEFAULT;
    }

    public String getBottomTitle() {
        return preferences.getData(Const.BOTTOM_TITLE, "");
    }

    public String getTopTitle() {
        return preferences.getData(Const.TOP_TITLE, "");
    }

    public boolean isArlertEvent() {
        return preferences.getData(Const.ARLERT_NOTIFCATION, Const.DEFAULT_ARLERT_NOTIFCATION);
    }

    public boolean isShowInStatusBar() {
        return preferences.getData(Const.SHOW_STATUS_BAR, Const.DEFAULT_SHOW_STATUS_BAR);
    }

    public int getColorBackground() {
        return preferences.getData(Const.COLOR_BACKGROUND, Const.DEFAULT_COLOR_BACKGROUND);
    }
}

