package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list.adapter;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;

public interface TimeListener {
    void showMenuCustomEvent(TimeModel timeModel);
}
