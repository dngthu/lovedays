package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.start;

import androidx.lifecycle.MutableLiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;

public class StartAppViewModel extends AbsViewModel {
    MutableLiveData<Long> startDate = new MutableLiveData<>();

    public LoveDayModel getCurrentLoveModel() {
        return getManager().getCurrentLoveModel();
    }

    public String getCurrentBackground() {
        return getManager().getCurrentBackground();
    }

    public void initLoveModel(long currentTimeMillis) {
        getManager().initLoveModel(currentTimeMillis);
    }

    public boolean isShowInStatusBar() {
        return getManager().isShowInStatusBar();
    }
}
