package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.BackgroundItemBinding;

public class BackgroundViewHolder extends RecyclerView.ViewHolder {
    private BackgroundListener listener;
    private BackgroundItemBinding binding;

    public static BackgroundViewHolder create(ViewGroup viewGroup, BackgroundListener listener) {
        BackgroundItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(viewGroup.getContext()),
                R.layout.background_item,
                viewGroup,
                false);
        return new BackgroundViewHolder(binding, listener);
    }

    public BackgroundViewHolder(@NonNull BackgroundItemBinding binding, BackgroundListener listener) {
        super(binding.getRoot());
        this.binding = binding;
        this.listener = listener;
    }

    public void onBind(final String path) {
        binding.imageV.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(itemView.getContext())
                .load(path)
                .into(binding.imageV);
        binding.imageV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSelectBackground(path);
            }
        });
    }

    public void onBindHeader() {
        binding.imageV.setScaleType(ImageView.ScaleType.FIT_CENTER);
        binding.imageV.setImageResource(R.drawable.ic_gallery);
        binding.imageV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSelectImageFromGallery();
            }
        });
    }
}
