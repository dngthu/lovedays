package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.model.DiaryEntity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

import java.util.List;

@Dao
public abstract class DiaryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(DiaryTable noteTable);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(List<DiaryContentTable> contentTables);

    @Query("DELETE FROM DiaryContentTable WHERE diaId=:diaryId")
    public abstract void removeContent(String diaryId);


    @Transaction
    public void insert(DiaryEntity entity){
        DiaryTable model = entity.getDiaryTable();
        insert(model);
        //clear old
        removeContent(model.getId());
        //insert new
        if (entity.getDiaryContents()!=null){
            insert(entity.getDiaryContents());
        }
    }

    @Query("SELECT * FROM DiaryTable N ORDER BY N.diaTimeCre DESC")
    public abstract LiveData<List<DiaryTable>> getDiaryTables();

    @Query("SELECT * FROM DiaryTable N WHERE N.diaTit LIKE :query ORDER BY N.diaTimeCre DESC")
    public abstract LiveData<List<DiaryTable>> getDiaryTables(String query);

    @Query("SELECT * FROM DiaryTable N WHERE N.diaTimeCre>=:start AND N.diaTimeCre<=:end ORDER BY N.diaTimeCre DESC")
    public abstract LiveData<List<DiaryTable>> getDiaryTables( Long start , Long end);

    @Query("SELECT * FROM DiaryTable N WHERE N.diaTit LIKE :query AND N.diaTimeCre>=:start AND N.diaTimeCre<=:end ORDER BY N.diaTimeCre DESC")
    public abstract LiveData<List<DiaryTable>> getDiaryTables(String query , Long start , Long end);

    @Query("SELECT * FROM DiaryTable N WHERE N.diaId=:id")
    public abstract LiveData<DiaryTable> getDiaryTable(String id);

    @Query("SELECT * FROM DiaryContentTable N WHERE N.diaId=:id ")
    public abstract LiveData<List<DiaryContentTable>> getListDiaryContent(String id);

    @Query("DELETE FROM DiaryTable  WHERE diaId=:id ")
    public abstract void deleteDiary(String id);
}
