package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.adapter;


import androidx.lifecycle.Observer;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

public interface DiaryContentListener {

    void observeDiary(Observer<DiaryTable> tableObserver);

}
