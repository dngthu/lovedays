package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity
public class DiaryContentTable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "conId")
    private String id = UUID.randomUUID().toString();
    @ColumnInfo(name = "diaId")
    private String diaryId;
    @ColumnInfo(name = "conTimeCre")
    private long timeCreate;
    @ColumnInfo(name = "conTex")
    private String content;
    @ColumnInfo(name = "conImg")
    private String imageUrl;
    @ColumnInfo(name = "conType")
    private int type;
    @ColumnInfo(name = "conTextStype")
    private int textStyle;
    @ColumnInfo(name = "conTextCenter")
    private boolean textCenter;

    @Ignore
    private static final int MAX_SUB_LEGHT = 100;

    @Ignore
    public DiaryContentTable(String content, int textStyle, boolean textCenter) {
        this.timeCreate = System.currentTimeMillis();
        this.content = content;
        this.imageUrl = null;
        this.type = DiaryContentType.TEXT;
        this.textStyle = textStyle;
        this.textCenter = textCenter;
    }

    @Ignore
    public DiaryContentTable(String imageUrl) {
        this.timeCreate = System.currentTimeMillis();
        this.content = null;
        this.imageUrl = imageUrl;
        this.type = DiaryContentType.IMAGE;
    }

    public DiaryContentTable(@NonNull String id, long timeCreate, String content, String imageUrl, int type, int textStyle, boolean textCenter) {
        this.id = id;
        this.timeCreate = timeCreate;
        this.content = content;
        this.imageUrl = imageUrl;
        this.type = type;
        this.textStyle = textStyle;
        this.textCenter = textCenter;
    }

    public void updateImage(String imageUrl) {
        this.timeCreate = System.currentTimeMillis();
        this.content = null;
        this.imageUrl = imageUrl;
        this.type = DiaryContentType.IMAGE;
    }

    public void updateText(String content, int textStyle, boolean textCenter) {
        this.timeCreate = System.currentTimeMillis();
        this.content = content;
        this.type = DiaryContentType.TEXT;
        this.textStyle = textStyle;
        this.textCenter = textCenter;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public long getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(int textStyle) {
        this.textStyle = textStyle;
    }

    public boolean isTextCenter() {
        return textCenter;
    }

    public void setTextCenter(boolean textCenter) {
        this.textCenter = textCenter;
    }

    public String getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(String diaryId) {
        this.diaryId = diaryId;
    }

    public String getSubContent() {
        if (content != null && !content.trim().isEmpty()) {
            if (content.length() > MAX_SUB_LEGHT) {
                return content.substring(0, MAX_SUB_LEGHT);
            } else {
                return content;
            }
        }
        return null;
    }
}
