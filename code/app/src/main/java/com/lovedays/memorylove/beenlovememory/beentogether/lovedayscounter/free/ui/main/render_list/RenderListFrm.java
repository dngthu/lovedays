package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsFragment;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.RenderListFrmBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.MainListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list.adapter.TimeAdapter;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list.adapter.TimeListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.AnimBuilder;

import java.util.List;

public class RenderListFrm extends AbsFragment<RenderListFrmBinding, RenderListViewModel> {

    private MainListener mainListener;

    public RenderListFrm(MainListener mainListener) {
        this.mainListener = mainListener;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.render_list_frm;
    }

    @Override
    protected Class<RenderListViewModel> getViewModelClass() {
        return RenderListViewModel.class;
    }


    private TimeAdapter timeAdapter;

    private TimeListener timeListener = new TimeListener() {
        @Override
        public void showMenuCustomEvent(TimeModel timeModel) {
            if (mainListener!=null) mainListener.showEventDialog(timeModel);
        }
    };

    @Override
    protected void initView() {

        initAdapter();
        bindAction();
    }

    private void bindAction() {
        binding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainListener != null) mainListener.addEventDay();
            }
        });
    }

    private void initAdapter() {

        timeAdapter = new TimeAdapter(timeListener);
        binding.listItem.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL,
                false));
        binding.listItem.setAdapter(timeAdapter);

        viewModel.getTimeModelList().observe(this, new Observer<List<TimeModel>>() {
            @Override
            public void onChanged(List<TimeModel> timeModels) {
                timeAdapter.updateList(timeModels);
                int scrollTo = viewModel.getIndextComming();
                if (scrollTo > 3) {
                    binding.listItem.scrollToPosition(scrollTo - 3);
                }
            }
        });

        binding.listItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Scrolling up
                    showAddButtom(false);
                } else {
                    // Scrolling down
                    showAddButtom(true);
                }
            }
        });

    }

    private void showAddButtom(boolean b) {
        AnimBuilder.Builder.createAl(binding.btnAdd)
                .hideDown()
                .show(b);
    }
}
