package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.UserLoveViewBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.UserLoveModel;

public class UserLoveView extends FrameLayout {

    private UserLoveDialog userLoveDialog;

    private UserLoveViewBinding binding;
    private UserLoveModel model;
    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void updateModel(UserLoveModel model) {
        this.model = model;
        updateLayout();
    }

    public UserLoveView(@NonNull Context context) {
        super(context);
        initView();
    }

    public UserLoveView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public UserLoveView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.user_love_view,
                this,
                false);
        addView(binding.getRoot(), LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        initWaveView();
        initDialog();
        bindAction();
    }

    private void initWaveView() {
        binding.waveLoadingView.setWaterLevelRatio(0.9f);
    }

    private void initDialog() {
        userLoveDialog = new UserLoveDialog(getContext());
    }

    private void bindAction() {
        binding.layoutUserA.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowMenuUser(true);
            }
        });

        binding.layoutUserB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowMenuUser(false);
            }
        });
    }

    private void onShowMenuUser(final boolean owner) {
        userLoveDialog.setListener(new UserLoveDialog.Listener() {
            @Override
            public void onChangeName() {
                String content = "";
                if (model != null) {
                    if (owner) {
                        content = model.getNameA();
                    } else {
                        content = model.getNameB();
                    }
                }

                if (listener != null) listener.onChangeName(owner, content);
            }

            @Override
            public void onChangeAvatar() {
                if (listener != null) listener.onChangeAvatar(owner);
            }

            @Override
            public void onRemoveAvatar() {
                if (listener != null) listener.onRemoveAvatar(owner);
            }
        });
        userLoveDialog.show();
    }

    private void updateLayout() {
        if (model != null) {
            Glide.with(getContext())
                    .load(model.getAvatarA())
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_avatar))
                    .apply(RequestOptions.errorOf(R.drawable.ic_avatar))
                    .into(binding.avatarUserA);
            Glide.with(getContext())
                    .load(model.getAvatarB())
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_avatar))
                    .apply(RequestOptions.errorOf(R.drawable.ic_avatar))
                    .into(binding.avatarUserB);
            String nameA = model.getNameA();
            String nameB = model.getNameB();

            if (!TextUtils.isEmpty(nameA)) {
                binding.nameUserA.setText(nameA);
            } else {
                binding.nameUserA.setText(getContext().getString(R.string.user_love_view_name_a_default));
            }
            if (!TextUtils.isEmpty(nameB)) {
                binding.nameUserB.setText(nameB);
            } else {
                binding.nameUserB.setText(getContext().getString(R.string.user_love_view_name_b_default));
            }
        } else {
            binding.nameUserA.setText(getContext().getString(R.string.user_love_view_name_a_default));
            binding.nameUserB.setText(getContext().getString(R.string.user_love_view_name_b_default));
        }
    }

    public interface Listener {
        void onChangeName(boolean owner, String content);

        void onChangeAvatar(boolean owner);

        void onRemoveAvatar(boolean owner);
    }
}
