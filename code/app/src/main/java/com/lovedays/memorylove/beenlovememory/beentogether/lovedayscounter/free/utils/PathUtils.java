package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.Serializable;
import java.util.Locale;

public class PathUtils {


    public static File getFileWithUri( Context context,  Uri uri){
        if (uri == null) return null;

        File  file = null;
        try {
            file = getFileLocal(context, uri);
            if (file != null && file.exists()) return file;
        } catch (  Exception e ) {
            e.printStackTrace();
        }

        if (file == null) {
            String path = getPathFile(context, uri);
            if (!TextUtils.isEmpty(path)) {
                file = new File(path);
                if (file.exists()) return file;
            }
            if (uri.getPath() != null) {
                file = new  File(uri.getPath());
                if (file.exists()) return file;
            }
            file = new File(uri.toString());
            if (file.exists()) return file;
            String pathFile = uri.toString();
            file = new File(pathFile);
            if (file.exists()) return file;
            String[] plits = pathFile.split(":");
            if (plits.length > 0) {
                file = new File(plits[plits.length - 1]);
                if (file.exists()) return file;
            }
            if (file.exists()) return file;
        }
        return file;
    }

    public static String getMimeType( Context context,  Uri uri) {
        String  mimeType  = null;
        try{
            if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
                ContentResolver cr = context.getContentResolver();
                mimeType = cr.getType(uri);
            } else {
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                        fileExtension.toLowerCase(Locale.ROOT)
                );
            }
        }catch ( Exception e){
            e.printStackTrace();
        }
        return mimeType;
    }

    public static Point getImageSize( Context context,  Uri uri) {
        int imageWidth = 0;
        int imageHeight = 0;
        try {
            if (uri!=null){
                BitmapFactory.Options options = new  BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(
                        context.getContentResolver().openInputStream(uri),
                        null,
                        options
                );
                imageHeight = options.outHeight;
                imageWidth = options.outWidth;

            }
        } catch ( java.lang.Exception e ) {
            e.printStackTrace();
        }
        return new Point(imageWidth, imageHeight);
    }

    private static File  getFileLocal(Context context, Uri uri) {
        if (uri != null) {
            String path = getPathFile(context, uri);
            if (isLocal(path)) {
                File file = new File(path);
                if (file.exists()) return file;
            }
        }
        return null;
    }

    private static boolean isLocal(String url) {
        return url != null && !url.startsWith("http://") && !url.startsWith("https://");
    }

    //Buison.  add method get FilePath
    public static String getPathFile (Context context, Uri uri) {
        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        String result  = null;
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) { // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                result = getPathExternalStorageDocumentFromURI(context, uri);
            } else if (isDownloadsDocument(uri)) {
                result = getPathDownloadDocumentFromURI(context, uri);
            } else if (isMediaDocument(uri)) {
                result =  getPathMediaDocumentFromURI(context, uri);
            }
            if(result ==null ) result  = getRealPathFromURI(context, uri);
            if(result ==null ) result  = getPathFromURI(context, uri);
            if(result ==null ) result  = getVideoPathFromURI(context, uri);
            if(result ==null ) result  = getImagePathFromURI(context, uri);
            if(result ==null ) result  = getAudioPathFromURI(context, uri);
            if(result ==null ) result  = getFilePathFromURI(context, uri);
        } else if ("content".equals(uri.getScheme())) {
            result =  getDataColumn(context, uri, null, null);
        } else if ("file".equals(uri.getScheme())) {
            result =  uri.getPath();
        }
        return result;
    }

    private static String getPathExternalStorageDocumentFromURI(Context context, Uri uri) {
        try {
            String docId = DocumentsContract.getDocumentId(uri);
            if (docId!= null) {
                if (docId.startsWith("raw:")){
                    return docId.replaceFirst("raw:", "");
                }
                String[] split = docId.split(":");
                String type="";
                if (split.length > 0){
                    type = split[0];
                }
                if ("primary".contentEquals(type)) {
                    return Environment.getExternalStorageDirectory().getPath() + "/" + split[1];
                }

            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getPathDownloadDocumentFromURI( Context context,  Uri uri){
        try {
            String id = DocumentsContract.getDocumentId(uri);
            if (id!= null){
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:", "");
                }
                if (id.startsWith("msf:")) {
                    String type = getMimeType(context, uri);
                    if (type!=null){
                        if (type.contains("image")){
                            return getImagePathFromURI(context, uri);
                        }else if ( type.contains("video")){
                            return getVideoPathFromURI(context, uri);
                        }else if (type.contains("audio")){
                            return getAudioPathFromURI(context, uri);
                        }
                    }
                    String path = getFilePathFromURI(context, uri);
                    if (path!=null && !path.isEmpty()){
                        return path;
                    }else {
                        return getPathFromURI(context, uri);
                    }
                }
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.parseLong(id));
                return getDataColumn(context, contentUri, null, null);
            }
        } catch ( java.lang.Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getPathMediaDocumentFromURI(Context context, Uri uri) {
        try {
            String docId = DocumentsContract.getDocumentId(uri);
            if (docId!=null){
                String[] split = docId.split(":");
                if (split.length > 1){
                    String type = split[0];
                    Uri  contentUri = null;
                    if (TextUtils.equals("image", type)){
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    }else if (TextUtils.equals("video", type)){
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    }else if (TextUtils.equals("audio", type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    String selection = "_id=?";
                    String[] selectionArgs = new String[]{split[1]};
                    return getDataColumn(
                            context,
                            contentUri,
                            selection,
                            selectionArgs
                    );

                }
            }
        } catch (java.lang.Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getPathFromURI( Context context,  Uri uri){
        String realPath = null;
        // SDK < API11
        try {
            if (Build.VERSION.SDK_INT < 11) {
                String[] proj = new String[]{MediaStore.Images.Media.DATA};
                @SuppressLint("Recycle")
                Cursor cursor = context.getContentResolver().query(uri, proj,
                        null, null, null);
                int column_index = 0;
                if (cursor != null) {
                    column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    realPath = cursor.getString(column_index);
                }
            } else if (Build.VERSION.SDK_INT < 19) {
                String[] proj = new String[]{MediaStore.Images.Media.DATA};
                CursorLoader cursorLoader = new CursorLoader(context, uri, proj, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                if (cursor != null) {
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    realPath = cursor.getString(column_index);
                }
            } else {
                String wholeID = DocumentsContract.getDocumentId(uri);
                // Split at colon, use second item in the array
                if (wholeID!=null){
                    String[] split = wholeID.split(":");
                    if (split.length>1){
                        String id = split[1];
                        String[] column = new String[]{MediaStore.Images.Media.DATA};
                        // where id is equal to
                        String sel = MediaStore.Images.Media._ID + "=?";
                        Cursor cursor = context.getContentResolver().query(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column,
                                sel,
                                new String[]{id},
                                null
                        );
                        int  columnIndex = 0;
                        if (cursor != null) {
                            columnIndex = cursor.getColumnIndex(column[0]);
                            if (cursor.moveToFirst()) {
                                realPath = cursor.getString(columnIndex);
                            }
                            cursor.close();
                        }
                    }
                }
            }
        } catch (java.lang.Exception  e ) {
            e.printStackTrace();
        }
        return realPath;
    }

    private static String getRealPathFromURI(Context context , Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = new String[]{MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor!=null){
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
        } catch ( java.lang.Exception e ) {
            e.printStackTrace();
        }  finally {
            if  (cursor!= null) cursor.close();
        }
        return null;
    }

    private static String getImagePathFromURI(Context context, Uri uri) {
        String  path = null;
        try {
            Cursor cursor = context.getContentResolver().query(uri,
                    null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                String document_id = cursor.getString(0);
                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                cursor.close();
                cursor = context.getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        null,
                        MediaStore.Images.Media._ID + " = ? ",
                        new String[]{document_id},
                        null
                );
                if (cursor != null) {
                    cursor.moveToFirst();
                    path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    cursor.close();
                }
            }
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    private static String getVideoPathFromURI( Context context,  Uri uri) {
        String path = null;
        try {
            Cursor cursor = context.getContentResolver().query(
                    uri,
                    null,
                    null,
                    null,
                    null
            );

            if (cursor != null) {
                cursor.moveToFirst();
                String document_id = cursor.getString(0);
                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                cursor.close();
                cursor = context.getContentResolver().query(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        null,
                        MediaStore.Video.Media._ID + " = ? ",
                        new String[]{document_id},
                        null
                );
                if (cursor != null) {
                    cursor.moveToFirst();
                    path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
                    cursor.close();
                }
            }
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    private static String getFilePathFromURI( Context context,  Uri uri) {
        String path = null;
        try {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                String document_id = cursor.getString(0);
                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                cursor.close();
                cursor = context.getContentResolver().query(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        null,
                        MediaStore.Audio.Media._ID + " = ? ",
                        new String[]{document_id},
                        null
                );
                if (cursor != null) {
                    cursor.moveToFirst();
                    path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    cursor.close();
                }
            }
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    private static String getAudioPathFromURI( Context context,  Uri uri){
        String path = null;
        try {
            Cursor cursor = context.getContentResolver().query(uri,
                    null, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();
                String document_id = cursor.getString(0);
                document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                cursor.close();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    cursor = context.getContentResolver().query(
                            MediaStore.Downloads.EXTERNAL_CONTENT_URI,
                            null,
                            MediaStore.Downloads._ID + " = ? ",
                            new String[]{document_id},
                            null);

                    if (cursor != null) {
                        cursor.moveToFirst();
                        path = cursor.getString(cursor.getColumnIndex(MediaStore.Downloads.DATA));
                        cursor.close();
                    }
                }
            }
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return path;
    }
    private static String getDataColumn( Context context,
                                  Uri uri,
                                  String selection,
                                  String[] selectionArgs) {
        Cursor cursor = null;
        try {
            String column = "_data";
            String[] projection = new String[]{column};

            cursor = context.getContentResolver().query(uri,
                    projection, selection, selectionArgs, null);

            if (cursor != null && cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } catch ( Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor!=null){
                cursor.close();
            }
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     * @author paulburke
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     * @author paulburke
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     * @author paulburke
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
