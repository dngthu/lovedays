package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.adapter;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.DiaryContentItemBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTextStyle;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentType;

import java.util.Calendar;

public class DiaryContentViewHolder extends RecyclerView.ViewHolder {

    @NonNull
    private DiaryContentItemBinding binding;

    public DiaryContentViewHolder(@NonNull DiaryContentItemBinding binding,
                                  @NonNull DiaryContentListener diaryContentListener) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public static DiaryContentViewHolder create(ViewGroup parent,
                                                DiaryContentListener diaryContentListener) {
        return new DiaryContentViewHolder(DiaryContentItemBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false),
                diaryContentListener);
    }

    public void bind(@NonNull DiaryContentTable diaryTable){
        if (diaryTable.getType() == DiaryContentType.TEXT){
            binding.imageV.setVisibility(View.GONE);
            binding.content.setVisibility(View.VISIBLE);
            bindText(diaryTable);
        }else if (diaryTable.getType() == DiaryContentType.IMAGE) {
            binding.imageV.setVisibility(View.VISIBLE);
            binding.content.setVisibility(View.GONE);
            bindImage(diaryTable);
        }else {
            binding.imageV.setVisibility(View.GONE);
            binding.content.setVisibility(View.GONE);
        }
    }

    private void bindImage(DiaryContentTable diaryTable) {
        Glide.with(itemView.getContext())
                .load(diaryTable.getImageUrl())
                .into(binding.imageV);
    }

    @SuppressLint("WrongConstant")
    private void bindText(@NonNull DiaryContentTable diaryTable) {
        binding.content.setGravity(diaryTable.isTextCenter()? Gravity.CENTER : Gravity.NO_GRAVITY);
        switch (diaryTable.getTextStyle()){
            case DiaryContentTextStyle.BOLD_ITALIC:
                binding.content.setTypeface(getTypeface(), Typeface.BOLD | Typeface.ITALIC);
                break;
            case DiaryContentTextStyle.BOLD:
                binding.content.setTypeface(getTypeface(), Typeface.BOLD);
                break;
            case DiaryContentTextStyle.ITALIC:
                binding.content.setTypeface(getTypeface(), Typeface.ITALIC);
                break;
            default:
                binding.content.setTypeface(getTypeface(), Typeface.NORMAL);
                break;
        }
        binding.content.setText(diaryTable.getContent());
    }

    @NonNull
    private Typeface getTypeface() {
        return Typeface.create(binding.content.getTypeface(), Typeface.NORMAL);
    }
}
