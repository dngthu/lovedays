package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils;

import android.Manifest;
import android.app.Activity;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.permission.RxPermissions;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FileUtils {

    public static void externalPermision(Activity activity,
                                         PerListener perListener) {

        RxPermissions rxPermissions = new RxPermissions(activity);

        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            perListener.onGranted();
                        } else {
                            perListener.onDeny();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public interface PerListener {
        void onGranted();
        void onDeny();
    }
}
