package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.DetailDiaryActBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.CreateDiaryAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.adapter.DiaryContentAdapter;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.adapter.DiaryContentListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.BitmapLoader;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.FileUtils;

public class DetailDiaryAct extends AbsActivity<DetailDiaryActBinding, DetailDiaryViewModel> {

    private static final String DIARY_ID = "DetailDiaryAct-DIARY_ID";

    @Override
    protected int getLayoutId() {
        return R.layout.detail_diary_act;
    }

    @Override
    protected Class<DetailDiaryViewModel> getViewModelClass() {
        return DetailDiaryViewModel.class;
    }

    @NonNull
    public static Intent getIntent(@NonNull Context context, @NonNull String id) {
        Intent intent = new Intent(context, DetailDiaryAct.class);
        intent.putExtra(DIARY_ID, id);
        return intent;
    }

    private DiaryContentAdapter diaryContentAdapter;

    private DiaryContentListener diaryContentListener = tableObserver -> {
        if (tableObserver != null) {
            viewModel.diaryTable.observe(DetailDiaryAct.this, tableObserver);
        }
    };


    @Override
    protected void initView() {
        setSupportActionBar(binding.toolBar);
        initIntent();
        initDiaryRec();
        initHandleDiary();
    }

    private void initDiaryRec() {
        diaryContentAdapter = new DiaryContentAdapter(diaryContentListener);
        binding.listItem.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.VERTICAL,
                false));
        binding.listItem.setAdapter(diaryContentAdapter);
    }

    private void initIntent() {
        String id = getIntent().getStringExtra(DIARY_ID);
        viewModel.diaryId.setValue(id);

    }

    private void initHandleDiary() {

        viewModel.diaryTable.observe(this, diaryTable ->
                binding.toolBar.setTitle(diaryTable!=null ?diaryTable.getTitle():"")
        );

        viewModel.contentTables.observe(this, contentTables -> {
            if (contentTables != null) {
                diaryContentAdapter.submitList(contentTables);
            }
        });
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail_diary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_action:
                onEditDiary();
                break;
            case R.id.share_action:
                onShareDiary();
                break;
            case R.id.delete_action:
                showAlertDeleteDiary();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAlertDeleteDiary() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.main_love_diary_dialog_delete_title)
                .setMessage(R.string.main_love_diary_dialog_delete_content)
                .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                    viewModel.deleteDiary();
                    finish();
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void onShareDiary() {
        try {
            FileUtils.externalPermision(DetailDiaryAct.this, new FileUtils.PerListener() {
                @Override
                public void onGranted() {
                    Bitmap bitmap = BitmapLoader.loadBitmapFromView(binding.layoutContainer);
                    if (bitmap != null) {
                        BitmapLoader.shareMore2(DetailDiaryAct.this, bitmap);
                    }
                }

                @Override
                public void onDeny() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onEditDiary() {
        String id = viewModel.diaryId.getValue();
        if (id != null && !id.isEmpty()) {
            startActivity(CreateDiaryAct.getIntentEdit(this, id));
        }
    }
}
