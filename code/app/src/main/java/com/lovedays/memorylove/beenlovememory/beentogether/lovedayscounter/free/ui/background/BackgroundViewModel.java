package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

import java.util.List;

public class BackgroundViewModel extends AbsViewModel {

    public void saveBackground(String path) {
        getManager().saveBackground(path);
    }

    public List<String> getListBackground() {
        return getManager().getListBackground();
    }
}
