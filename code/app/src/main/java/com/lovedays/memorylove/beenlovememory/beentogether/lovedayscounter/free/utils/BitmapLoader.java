package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageDecoder;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.BuildConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Random;

public class BitmapLoader {

    public static void shareMore(Context context, Bitmap bitmap) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "img_share.jpg");

        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            Uri photoURI = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider", f);
            share.putExtra("android.intent.extra.STREAM", photoURI);
            context.startActivity(Intent.createChooser(share, "Share this photo"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void shareMore2(Context context, Bitmap bitmap) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(getStoreSD(context)+ "img_share.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

            Uri photoURI = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider", f);
            share.putExtra("android.intent.extra.STREAM", photoURI);
            context.startActivity(Intent.createChooser(share, "Share this photo"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getStoreSD(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            File f = c.getExternalFilesDir(null);
            if (f != null)
                return f.getAbsolutePath();
            else
                return "/storage/emulated/0/Android/data/" + c.getPackageName();
        } else {
            return Environment.getExternalStorageDirectory()
                    + "/Android/data/" + c.getPackageName();
        }
    }

    public static void shareBitmap(Context context, Bitmap bitmap) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "img_share.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

            Uri photoURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", f);
            share.putExtra("android.intent.extra.STREAM", photoURI);
            context.startActivity(Intent.createChooser(share, "Share this photo"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Bitmap loadBitmapUri(@NonNull Context context, String imageUri) {
        try {
            Bitmap result;
            if (Build.VERSION.SDK_INT < 28) {
                result = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(imageUri));
            } else {
                ImageDecoder.Source source = ImageDecoder.createSource(context.getContentResolver(), Uri.parse(imageUri));
                result = ImageDecoder.decodeBitmap(source);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap loadBitmapFromView(View v) {
        try {
            Bitmap b = Bitmap.createBitmap(v.getWidth(),
                    v.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
            v.draw(c);
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Uri getImageUri(Context inContext, Bitmap mBitmap) {
        Uri uri = null;
        try {
            File file = new File(inContext.getFilesDir(), "Image"
                    + new Random().nextInt() + ".jpeg");
            FileOutputStream out = inContext.openFileOutput(file.getName(),
                    Context.MODE_PRIVATE);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            //get absolute path
            String realPath = file.getAbsolutePath();
            File f = new File(realPath);
            uri = Uri.fromFile(f);
        } catch (Exception e) {
            Log.e("Your Error Message", Objects.requireNonNull(e.getMessage()));
        }
        return uri;

    }
}
