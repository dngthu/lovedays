package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.CreateDiaryImageViewBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;

public class CreateDiaryImageView extends FrameLayout {
    public CreateDiaryImageView(@NonNull Context context) {
        super(context);
        initView();
    }

    public CreateDiaryImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CreateDiaryImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private CreateDiaryImageViewBinding binding;

    private CreateDiaryListener createDiaryListener;

    public void setCreateDiaryListener(CreateDiaryListener createDiaryListener) {
        this.createDiaryListener = createDiaryListener;
    }

    private void initView() {
        binding = CreateDiaryImageViewBinding.inflate(LayoutInflater.from(getContext()));
        addView(binding.getRoot(), LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        initActions();
    }

    private void initActions() {
        binding.removeBtn.setOnClickListener(view -> {
            if (createDiaryListener != null) {
                createDiaryListener.onRemoveImage(CreateDiaryImageView.this);
            }
        });
    }

    private Uri currentUri;

    private DiaryContentTable table;

    public void updateImage(Uri uri) {
        currentUri = uri;
        loadImage();
    }


    public void updateModel(DiaryContentTable table) {
        this.table = table;
        String uri = table.getImageUrl();
        if (uri!=null && !uri.isEmpty()){
            currentUri = Uri.parse(uri);
            loadImage();
        }
    }

    private void loadImage() {
        Glide.with(this)
                .load(currentUri)
                .into(binding.imageV);
    }

    public DiaryContentTable getContent() {
        if (currentUri != null) {
            if (table!=null){
                table.updateImage(currentUri.toString());
                return table;
            }
            return new DiaryContentTable(currentUri.toString());
        }
        return null;
    }

}