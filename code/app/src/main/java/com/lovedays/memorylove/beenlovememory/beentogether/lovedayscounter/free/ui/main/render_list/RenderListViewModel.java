package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

import java.util.List;

public class RenderListViewModel extends AbsViewModel {


    private LiveData<List<TimeModel>> list;

    public RenderListViewModel() {
        list = getManager().getTimeModelList();

    }

    public int getIndextComming() {
        return getManager().getIndextComming();
    }

    public LiveData<List<TimeModel>> getTimeModelList() {
        return list;
    }
}
