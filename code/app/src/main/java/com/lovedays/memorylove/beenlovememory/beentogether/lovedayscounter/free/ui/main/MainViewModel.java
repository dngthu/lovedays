package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main;

import androidx.annotation.NonNull;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.EventModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.UserLoveModel;

public class MainViewModel extends AbsViewModel {



    public void onRemoveAvatarUserModel(boolean owner) {
        UserLoveModel model = getManager().getUserLoveModel();
        if (owner) {
            model.setAvatarA("");
        } else {
            model.setAvatarB("");
        }
        getManager().updateUserLoveModel(model);
        getManager().updateUserLoveModel(model);
    }

    public void onUpdateAvatarUser(String content, boolean owner) {
        UserLoveModel model = getManager().getUserLoveModel();
        if (owner){
            model.setAvatarA(content);
        }else {
            model.setAvatarB(content);
        }
        getManager().updateUserLoveModel(model);
    }

    public void onUpdateNameUser(String content, boolean owner) {
        UserLoveModel model = getManager().getUserLoveModel();
        if (owner){
            model.setNameA(content);
        }else{
            model.setNameB(content);
        }
        getManager().updateUserLoveModel(model);
    }

    public void saveTopTitle(String content) {
        getManager().saveTopTitle(content);
    }

    public void saveBottomTitle(String content) {
        getManager().saveBottomTitle(content);
    }

    public String getCurrentBackground() {
        return getManager().getCurrentBackground();
    }

    public void updateTimeLoveModel(long timeInMillis) {
        getManager().changeTimeLoveModel(timeInMillis);
    }

    public void addEventModel(@NonNull EventModel model) {
        getManager().addEventModel(model);
    }

    public String getTopTitle() {
        return getManager().getTopTitle();
    }

    public String getBottomTitle() {
        return getManager().getBottomTitle();
    }

    public LoveDayModel getCurrentLoveModel() {
        return getManager().getCurrentLoveModel();
    }

    public void onRemoveEvent(String whatId) {
        getManager().removeEvent(whatId);
    }

    public void onStart0Changed(boolean b) {
        LoveDayModel model = getManager().getCurrentLoveModel();
        if (model!=null){
            model.setStartDay0(b);
            getManager().updateLoveModel(model);
        }
    }

    public void onShowYearChanged(boolean b) {
        LoveDayModel model = getManager().getCurrentLoveModel();
        if (model!=null){
            model.setShowYear(b);
            getManager().updateLoveModel(model);
        }
    }

    public void onShow100Day(boolean b) {
        LoveDayModel model = getManager().getCurrentLoveModel();
        if (model!=null){
            model.setShow100Day(b);
            getManager().updateLoveModel(model);
        }
    }

    public void onArlertEventChanged(boolean b) {
        getManager().saveArlertEvent(b);
    }

    public void setBackgroundColor(int selectedColor) {
        getManager().saveColorBackground(selectedColor);
    }

    public void onShowInStatusBar(boolean b){
        getManager().saveShowInStatusBar(b);
    }
}

