package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.MainLoveDialogBinding;

public class MainLoveDialog extends BottomSheetDialog {
    private MainLoveDialogListener listener;
    private MainLoveDialogBinding binding;

    public MainLoveDialog(@NonNull Context context) {
        super(context);
        initView();
    }

    public MainLoveDialog(@NonNull Context context, int theme) {
        super(context, theme);
        initView();
    }

    public void setListener(MainLoveDialogListener listener) {
        this.listener = listener;
    }

    private void initView() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.main_love_dialog,
                null, false);
        setContentView(binding.getRoot());
        bindAction();
    }

    private void bindAction() {
        binding.changeDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onChangeDate();
                dismiss();
            }
        });

        binding.changeTopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onChangeTopTitle();
                dismiss();
            }
        });

        binding.changeBottomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onChangeBottomTitle();
                dismiss();
            }
        });

        binding.changeBgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onChangeBackground();
                dismiss();
            }
        });

        binding.shareScBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onShareScreenShort();
                dismiss();
            }
        });
    }
}
