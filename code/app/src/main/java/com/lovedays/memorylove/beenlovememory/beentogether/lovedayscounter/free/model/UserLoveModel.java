package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserLoveModel implements Parcelable {
    private String avatarA;
    private String avatarB;
    private String nameA;
    private String nameB;


    public UserLoveModel(String avatarA, String avatarB, String nameA, String nameB) {
        this.avatarA = avatarA;
        this.avatarB = avatarB;
        this.nameA = nameA;
        this.nameB = nameB;
    }

    public static UserLoveModel empty() {
        return new UserLoveModel("","","","");
    }

    public void setAvatarA(String avatarA) {
        this.avatarA = avatarA;
    }

    public void setAvatarB(String avatarB) {
        this.avatarB = avatarB;
    }

    public void setNameA(String nameA) {
        this.nameA = nameA;
    }

    public void setNameB(String nameB) {
        this.nameB = nameB;
    }

    public String getAvatarA() {
        return avatarA;
    }

    public String getAvatarB() {
        return avatarB;
    }

    public String getNameA() {
        return nameA;
    }

    public String getNameB() {
        return nameB;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.avatarA);
        dest.writeString(this.avatarB);
        dest.writeString(this.nameA);
        dest.writeString(this.nameB);
    }

    public UserLoveModel() {
    }

    protected UserLoveModel(Parcel in) {
        this.avatarA = in.readString();
        this.avatarB = in.readString();
        this.nameA = in.readString();
        this.nameB = in.readString();
    }

    public static final Parcelable.Creator<UserLoveModel> CREATOR = new Parcelable.Creator<UserLoveModel>() {
        @Override
        public UserLoveModel createFromParcel(Parcel source) {
            return new UserLoveModel(source);
        }

        @Override
        public UserLoveModel[] newArray(int size) {
            return new UserLoveModel[size];
        }
    };
}
