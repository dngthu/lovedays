package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

import java.util.List;

public class DetailDiaryViewModel extends AbsViewModel {

    MutableLiveData<String> diaryId = new MutableLiveData<>();

    LiveData<DiaryTable> diaryTable ;

    LiveData<List<DiaryContentTable>> contentTables ;

    public DetailDiaryViewModel() {
        diaryTable = Transformations.switchMap(diaryId, input -> {
            if (input!=null && !input.isEmpty()){
                return getDiary().getDiaryTable(input);
            }
            return new MutableLiveData<>();
        });

        contentTables = Transformations.switchMap(diaryId, input -> {
            if (input!=null && !input.isEmpty()){
                return getDiary().getListDiaryContent(input);
            }
            return new MutableLiveData<>();
        });
    }

    public void deleteDiary() {
        getDiary().deleteDiary(diaryId.getValue());
    }
}
