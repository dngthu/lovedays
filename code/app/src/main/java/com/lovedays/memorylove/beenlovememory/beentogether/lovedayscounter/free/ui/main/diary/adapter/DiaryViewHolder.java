package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.DiaryItemBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.Strings;

import java.util.Calendar;

public class DiaryViewHolder extends RecyclerView.ViewHolder {

    @NonNull
    private DiaryItemBinding binding;
    @NonNull
    private DiaryListener diaryListener;

    public DiaryViewHolder(@NonNull DiaryItemBinding binding,
                           @NonNull DiaryListener diaryListener) {
        super(binding.getRoot());
        this.binding = binding;
        this.diaryListener = diaryListener;
    }

    public static DiaryViewHolder create(ViewGroup parent, DiaryListener diaryListener) {
        return new DiaryViewHolder(DiaryItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false),
                diaryListener);
    }

    public void bind(@NonNull DiaryTable diaryTable){
        binding.titleTv.setText(diaryTable.getTitle());
        String des = diaryTable.getDesciption();
        if (des!=null && !des.isEmpty()){
            binding.content.setText(des);
        }else {
            binding.content.setText(R.string.main_love_diary_item_not_have_content);
        }

        itemView.setOnClickListener(view -> diaryListener.detailDiary(diaryTable));
        bindDate(diaryTable);

    }

    private void bindDate(@NonNull DiaryTable diaryTable) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(diaryTable.getTimeCreate());
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        binding.dayTv.setText(String.valueOf(day));
        binding.dateTv.setText(String.format("%s-%s", month, year));
    }
}
