package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;

import java.util.ArrayList;
import java.util.List;

public class TimeAdapter extends RecyclerView.Adapter<TimeHolder> {
    private TimeListener timeListener;

    private List<TimeModel> timeModelList;

    public TimeAdapter(TimeListener timeListener) {
        this.timeModelList = new ArrayList<>();
        this.timeListener = timeListener;
    }

    public void updateList(@NonNull List<TimeModel> timeModelList) {
        this.timeModelList = timeModelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return TimeHolder.create(parent,  timeListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeHolder holder, int position) {

        if (position<timeModelList.size()){
            holder.onBind(timeModelList.get(position));

        }else{
            holder.onBindSpace();
        }
    }

    @Override
    public int getItemCount() {
        return timeModelList.size() +1 ;
    }
}
