package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;

public interface CreateDiaryListener  {

    LifecycleOwner getLifecycleOwner();

    void onRemoveText(@NonNull CreateDiaryTextView createDiaryTextView);

    void onRemoveImage(@NonNull CreateDiaryImageView createDiaryImageView);

    void onLoadCover();

    void addText();

    void addImage();

}
