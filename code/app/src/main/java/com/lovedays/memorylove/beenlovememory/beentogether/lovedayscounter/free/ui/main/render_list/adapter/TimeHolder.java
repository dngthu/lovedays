package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.TimeItemBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;


public class TimeHolder extends RecyclerView.ViewHolder {
    private TimeItemBinding binding;
    private TimeListener timeListener;

    public static TimeHolder create(@NonNull ViewGroup viewGroup, TimeListener timeListener) {
        TimeItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.time_item,
                viewGroup,
                false);
        return new TimeHolder(binding, timeListener);
    }

    public TimeHolder(@NonNull TimeItemBinding binding, TimeListener timeListener) {
        super(binding.getRoot());
        this.binding = binding;
        this.timeListener = timeListener;
    }

    public void onBind(final TimeModel timeModel) {
        binding.container.setVisibility(View.VISIBLE);
        binding.dateTv.setText(timeModel.getDate());
        if (timeModel.isCustomEvent()) {
            binding.titleTv.setText(String.format("* %s", timeModel.getTitle()));
        } else {
            binding.titleTv.setText(timeModel.getTitle());
        }

        int count = timeModel.getCount();
        if (count == 0) {
            binding.countDayTv.setText("D-Day");
        } else if (count > 0) {
            binding.countDayTv.setText(String.format("D+%d", Math.abs(count)));
        } else {
            binding.countDayTv.setText(String.format("D-%d", Math.abs(count)));
        }

        if (timeModel.isComming()) {
            binding.icComming.setVisibility(View.VISIBLE);
            binding.tvComming.setVisibility(View.VISIBLE);
        } else {
            binding.icComming.setVisibility(View.INVISIBLE);
            binding.tvComming.setVisibility(View.GONE);
        }

        if (timeModel.getCount() > 0) {
            binding.getRoot().setAlpha(0.5f);
        } else {
            binding.getRoot().setAlpha(1f);
        }
        if (timeModel.isCustomEvent()) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (timeListener != null) timeListener.showMenuCustomEvent(timeModel);
                }
            });
        } else {
            itemView.setOnClickListener(null);
        }
    }

    public void onBindSpace() {
        binding.container.setVisibility(View.INVISIBLE);
    }
}
