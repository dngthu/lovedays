package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free;

import android.app.Application;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.util.DisplayMetrics;

import androidx.annotation.NonNull;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.cache.Preferences;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.cache.PreferencesImpl;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayManager;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.receiver.AlarmUtils;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.receiver.OpenPhoneService;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.start.StartAppAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.NotificationUtils;

public class LoveDayApp extends Application {
    public static int WIDTH_SCREEN;
    public static int HEIGHT_SCREEN;
    private static LoveDayApp contextApp;

    private Preferences sharedPreferences;

    public static LoveDayApp getInstance() {
        return contextApp;
    }

    @NonNull
    public Preferences getSharedPre() {
        if (sharedPreferences == null) sharedPreferences = PreferencesImpl.getInstance();
        return sharedPreferences;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        contextApp = this;
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        WIDTH_SCREEN = Math.min(width, height);
        HEIGHT_SCREEN = Math.max(width, height);
        AlarmUtils.create(this);
    }

    public void resetAllData(){
        LoveDayManager.reset();
        sharedPreferences.clear();
        sendBroadcastUpdateWidget();
        startActivity(StartAppAct.getIntent(this));
    }

    public void sendBroadcastUpdateWidget() {
        refreshNotifiStatusBar();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent();
                i.setAction(Const.ACTION_UPDATE_WIDGET);
                LoveDayApp.getInstance().sendBroadcast(i);
            }
        });
    }

    public void refreshNotifiStatusBar(){
        LoveDayManager manager = LoveDayManager.getManager(getSharedPre());
        if (manager.isShowInStatusBar()){
            NotificationUtils.buildNotificationStatusBar(this);
        }else {
            NotificationUtils.clearNotificationStatusBar(this);
        }
    }
}
