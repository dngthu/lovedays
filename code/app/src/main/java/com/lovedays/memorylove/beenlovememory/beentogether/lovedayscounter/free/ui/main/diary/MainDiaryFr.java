package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.MainDiaryFrBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsFragment;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter.DiaryAdapter;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter.DiaryHeaderViewHolder;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter.DiaryListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.CreateDiaryAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.DetailDiaryAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.AnimBuilder;

public class MainDiaryFr extends AbsFragment<MainDiaryFrBinding, MainDiaryViewModel> {
    @Override
    protected int getLayoutId() {
        return R.layout.main_diary_fr;
    }

    @Override
    protected Class<MainDiaryViewModel> getViewModelClass() {
        return MainDiaryViewModel.class;
    }


    private DiaryAdapter diaryAdapter;

    private DiaryListener diaryListener = new DiaryListener() {
        @Override
        public void onCreateDiary() {
            startActivity(CreateDiaryAct.getIntent(requireContext()));
        }

        @Override
        public void detailDiary(@NonNull DiaryTable diaryTable) {
            startActivity(DetailDiaryAct.getIntent(requireContext(), diaryTable.getId()));
        }
    };


    @Override
    protected void initView() {

        initRecycle();
        initHandleDiaryList();
        bindActions();
    }

    private void bindActions() {
        binding.btnAdd.setOnClickListener(view -> diaryListener.onCreateDiary());
    }

    private void initHandleDiaryList() {

        viewModel.listDiary.observe(this, diaryTables -> {
            if (diaryTables!=null){
                diaryAdapter.submitList(diaryTables);
            }
        });

    }

    private void initRecycle() {
        diaryAdapter = new DiaryAdapter(diaryListener);
        binding.listDiary.setLayoutManager(new LinearLayoutManager(requireContext(),
                RecyclerView.VERTICAL,
                false));

        binding.listDiary.setAdapter(diaryAdapter);
        binding.listDiary.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                View firstView = recyclerView.getChildAt(0);
                if (firstView!=null){
                    RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(firstView);
                    if (viewHolder!= null ){
                        showAddButtom(!(viewHolder instanceof DiaryHeaderViewHolder));
                    }
                }
            }
        });
    }

    private void showAddButtom(boolean b) {
        AnimBuilder.Builder.createAl(binding.btnAdd)
                .hideDown()
                .show(b);
    }
}
