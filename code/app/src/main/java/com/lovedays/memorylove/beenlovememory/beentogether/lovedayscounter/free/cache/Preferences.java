package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.cache;

import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;

public interface Preferences {
    void registerChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener);

    void ungisterChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener);

    <T> T getData(String key, T def, Class<T> c);

    <T> T getData(String key, T def);

    <T> T getObject(String key, Class<T> c);

    <T> void putData(String key, T value);

    void remove(String key);

    void clear();

    <T> LiveData<T> getDataLive(String key, T def);
}
