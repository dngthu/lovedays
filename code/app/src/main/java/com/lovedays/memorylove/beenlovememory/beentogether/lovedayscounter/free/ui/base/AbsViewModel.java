package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.cache.Preferences;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.respository.DiaryRepository;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.respository.DiaryRepositoryImpl;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayManager;

public abstract class AbsViewModel extends ViewModel {

    @NonNull
    protected Preferences getSharePre() {
        return LoveDayApp.getInstance().getSharedPre();
    }

    @NonNull
    protected LoveDayManager getManager() {
        return LoveDayManager.getManager(getSharePre());
    }

    protected DiaryRepository getDiary() {
        return DiaryRepositoryImpl.getInstance(LoveDayApp.getInstance());
    }
}
