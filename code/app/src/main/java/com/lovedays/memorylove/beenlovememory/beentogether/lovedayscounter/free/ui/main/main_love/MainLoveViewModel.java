package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.main_love;

import androidx.lifecycle.LiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.UserLoveModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

import java.util.List;

public class MainLoveViewModel extends AbsViewModel {
    public UserLoveModel getUserLoveModel() {
        return getManager().getUserLoveModel();
    }

    public String getTopTitle() {
        return getManager().getTopTitle();
    }

    public String getBottomTitle() {
        return getManager().getBottomTitle();
    }

    public LiveData<List<TimeModel>> getTimeModelList() {
        return getManager().getTimeModelList();
    }

    public TimeModel getNextTimeModel(){
        return getManager().getNextTimeModel();
    }


    public LoveDayModel getCurrentLoveModel() {
        return getManager().getCurrentLoveModel();
    }

    public int getIndextComming() {
        return getManager().getIndextComming();
    }
}
