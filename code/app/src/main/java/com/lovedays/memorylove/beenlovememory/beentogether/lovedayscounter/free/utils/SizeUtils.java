package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils;

import android.content.Context;
import android.util.TypedValue;

import androidx.annotation.NonNull;

public class SizeUtils {

    public static float pxToDp(@NonNull Context context,
                                 float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float dpToPx(@NonNull Context context,
                                 float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
    public static int spToPx(@NonNull Context context, float sp ) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                sp, context.getResources().getDisplayMetrics());
    }

}
