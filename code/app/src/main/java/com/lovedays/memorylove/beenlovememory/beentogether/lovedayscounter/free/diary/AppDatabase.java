package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.dao.DiaryDao;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;


@Database(entities = {DiaryTable.class,
        DiaryContentTable.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static final String LOVEDAYS_DATABASE = "LOVEDAYS_DATABASE";
    private static AppDatabase appDatabase;

    @NonNull
    public static AppDatabase getInstance(Context context){
        if (appDatabase==null){
            appDatabase = Room.databaseBuilder(context, AppDatabase.class, LOVEDAYS_DATABASE)
                    .allowMainThreadQueries()
                    .build();
        }
        return appDatabase;
    }

    public abstract DiaryDao getNoteDao();

}
