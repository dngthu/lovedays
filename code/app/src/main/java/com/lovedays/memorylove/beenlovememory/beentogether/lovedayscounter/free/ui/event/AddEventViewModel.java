package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.event;

import androidx.lifecycle.MutableLiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.EventModel;

public class AddEventViewModel extends AbsViewModel {
    MutableLiveData<Long> startDate = new MutableLiveData<>();
    MutableLiveData<EventModel> editModel = new MutableLiveData<>();

    public EventModel getEventModel(String id) {
        return getManager().getEventModel(id);
    }

    public String getCurrentBackground() {
        return getManager().getCurrentBackground();
    }
}
