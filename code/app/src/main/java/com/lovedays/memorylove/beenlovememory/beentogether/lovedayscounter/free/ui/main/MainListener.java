package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;

public interface MainListener {

    void addEventDay();
    void showMainLoveDialog();

    void showEventDialog(TimeModel timeModel);

    void onChangeName(boolean owner, String content);
    void onChangeAvatar(boolean owner);
    void onRemoveAvatar(boolean owner);

    void navigateBackground();

    void onStart0Changed(boolean b);
    void onShowYearChanged(boolean b);
    void onShow100Day(boolean b);

    void onArlertEventChanged(boolean b);
    void onShowInStatusBar(boolean b);
    void setBackGroundColor(int selectedColor);

    void resetAllData();
}
