package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.main_love;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsFragment;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.MainLoveFrmBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.MainListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.view.UserLoveView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainLoveFrm extends AbsFragment<MainLoveFrmBinding, MainLoveViewModel> {

    private MainListener mainListener;

    public MainLoveFrm(MainListener mainListener) {
        this.mainListener = mainListener;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.main_love_frm;
    }

    @Override
    protected Class<MainLoveViewModel> getViewModelClass() {
        return MainLoveViewModel.class;
    }


    @Override
    protected void initView() {
        initCountLove();
        initHanleListModel();
        updateLayoutTitle();
        initUserLoveView();
    }

    private void initHanleListModel() {

        viewModel.getTimeModelList().observe(this, new Observer<List<TimeModel>>() {
            @Override
            public void onChanged(List<TimeModel> timeModels) {
                updateCountLove(viewModel.getNextTimeModel());
            }
        });
    }

    private void initUserLoveView() {

        binding.userLoveView.updateModel(viewModel.getUserLoveModel());

        binding.userLoveView.setListener(new UserLoveView.Listener() {
            @Override
            public void onChangeName(boolean owner, String content) {
                if (mainListener != null) mainListener.onChangeName(owner, content);
            }

            @Override
            public void onChangeAvatar(boolean owner) {
                if (mainListener != null) mainListener.onChangeAvatar(owner);
            }

            @Override
            public void onRemoveAvatar(boolean owner) {
                if (mainListener != null) mainListener.onRemoveAvatar(owner);
            }
        });
    }

    public void updateLayoutTitle() {
        if (binding != null && binding.countLoveView != null) {
            binding.countLoveView.setLayoutTitle(viewModel.getTopTitle(),
                    viewModel.getBottomTitle());
        }
    }

    public void refreshUserModel() {
        if (binding != null && binding.userLoveView != null) {
            initUserLoveView();
        }
    }

    private void initCountLove() {
        LoveDayModel model = viewModel.getCurrentLoveModel();
        binding.countLoveView.setLoveDayModel(model);

        binding.countLoveView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainListener != null) mainListener.showMainLoveDialog();
            }
        });
    }

    private void updateCountLove(TimeModel timeModel) {

        LoveDayModel model = viewModel.getCurrentLoveModel();

        Calendar calendar = Calendar.getInstance();
        if (timeModel != null) {
            calendar.setTimeInMillis(timeModel.getTime());
        } else {
            calendar.setTimeInMillis(DateUtils.toDay());
        }
        long nextTime = calendar.getTimeInMillis();

        binding.countLoveView.setLoveDayModel(model);
        int maxDay = DateUtils.getCountDay(model.isStartDay0(), model.getStartTime(), nextTime);
        binding.countLoveView.rangeProgressDate(Math.abs(maxDay));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshAnimation();
    }

    public void refreshAnimation() {
        if (binding != null && binding.countLoveView != null) {
            binding.countLoveView.refreshAnimation();
        }
    }
}
