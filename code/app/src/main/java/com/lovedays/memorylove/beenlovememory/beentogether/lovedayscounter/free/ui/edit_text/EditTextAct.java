package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.edit_text;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.view.View;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.EditTextActBinding;


public class EditTextAct extends AbsActivity<EditTextActBinding, EditTexViewModel> {

    private static final String CONTENT_EDIT = "EditTextAct-CONTENT_EDIT";

    public enum EditType {
        TOP_TITLE, BOT_TITLE, USER_NAME_A, USER_NAME_B
    }

    public static Intent getIntent(@NonNull Context context, @NonNull EditType editType, String content) {
        Intent intent = new Intent(context, EditTextAct.class);
        intent.putExtra(EditType.class.getName(), editType);
        intent.putExtra(CONTENT_EDIT, content);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.edit_text_act;
    }

    @Override
    protected Class<EditTexViewModel> getViewModelClass() {
        return EditTexViewModel.class;
    }

    @Override
    protected void initView() {
        initCurrentText();
        initBackground();
        bindAction();
    }

    private void initCurrentText() {
        if (getIntent() != null) {
            EditType editType = (EditType) getIntent().getSerializableExtra(EditType.class.getName());
            String content = getIntent().getStringExtra(CONTENT_EDIT);
            if (content == null) content = "";
            if (editType != null) {
                binding.editTitle.setText(content);
                binding.editTitle.setSelection(content.length());
                switch (editType){
                    case BOT_TITLE:
                        binding.editTitle.setHint(R.string.edit_text_hint_top_title);
                        break;
                    case TOP_TITLE:
                        binding.editTitle.setHint(R.string.edit_text_hint_bottom_title);
                        break;
                    case USER_NAME_A:
                        binding.editTitle.setHint(R.string.edit_text_hint_your_name);
                        break;
                    case USER_NAME_B:
                        binding.editTitle.setHint(R.string.edit_text_hint_partner_name);
                        break;
                }
            }
        }
    }

    private void initBackground() {
        Glide.with(this)
                .load(viewModel.getCurrentBackground())
                .into(binding.imageBg);
    }

    private void bindAction() {

        binding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Editable edt = binding.editTitle.getText();
                String str = edt == null ? "" : edt.toString().trim();
                if (str.isEmpty()) {
                    binding.editTitle.setError(getString(R.string.add_text_input_error));
                } else {
                    initResult(str);
                }
            }
        });
    }

    private void initResult(String str) {
        Intent intent = new Intent();
        intent.putExtra(String.class.getName(), str);
        setResult(RESULT_OK, intent);
        finish();
    }
}
