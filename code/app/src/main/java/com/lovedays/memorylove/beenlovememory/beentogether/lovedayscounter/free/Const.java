package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free;

public final class Const {

    //asset
    public static final String BG_ASSETS_FOLDER = "bg";
    public static final String ASSETS_FOLDER = "file:///android_asset/";

    //key fersharefence
    public static final String START_LOVE_DAY = "START_LOVE_DAY";

    public static final String COLOR_BACKGROUND = "COLOR_BACKGROUND";
    public static final int DEFAULT_COLOR_BACKGROUND = 0xFFE880BD;

    public static final String SHOW_STATUS_BAR = "SHOW_STATUS_BAR";
    public static final Boolean DEFAULT_SHOW_STATUS_BAR = true;

    public static final String ARLERT_NOTIFCATION = "ARLERT_NOTIFCATION";
    public static final Boolean DEFAULT_ARLERT_NOTIFCATION = true;

    public static final String BACKGROUND = "BACKGROUND";
    public static final String BACKGROUND_DEFAULT = "file:///android_asset/bg/image5.jpg";

    public static final String EVENT_MODELS = "EVENT_MODELS";
    public static final String TOP_TITLE = "TOP_TITLE";
    public static final String BOTTOM_TITLE = "BOTTOM_TITLE";
    public static final String USER_LOVE_MODEL = "USER_LOVE_MODEL";

    public static final String DIARY_COVER_DEFAULT = "file:///android_asset/bg/Image_cover.jpg";
    //action
    public static final String ACTION_UPDATE_WIDGET = "android.appwidget.action.APPWIDGET_UPDATE";
}
