package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog;

public interface MainLoveDialogListener {

    public void onChangeDate();

    public void onChangeTopTitle();

    public void onChangeBottomTitle();

    public void onChangeBackground();

    public void onShareScreenShort();

}
