package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.receiver;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.cache.Preferences;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayManager;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.NotificationUtils;

public class OpenPhoneService extends IntentService {

    public OpenPhoneService() {
        super(OpenPhoneService.class.getSimpleName());
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, OpenPhoneService.class);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            new Handler(Looper.getMainLooper()).post(() -> {
                Preferences sha = LoveDayApp.getInstance().getSharedPre();
                LoveDayManager manager = LoveDayManager.getManager(sha);
                if (manager.isArlertEvent()) {
                    NotificationUtils.buildNotification(OpenPhoneService.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
