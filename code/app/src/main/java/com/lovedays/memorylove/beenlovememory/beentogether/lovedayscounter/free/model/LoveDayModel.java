package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model;

public class LoveDayModel {

    private long startTime;
    private boolean startDay0;
    private boolean show100Day;
    private boolean showYear;

    public void setShow100Day(boolean show100Day) {
        this.show100Day = show100Day;
    }

    public boolean isShow100Day() {
        return show100Day;
    }

    public boolean isShowYear() {
        return showYear;
    }

    public void setShowYear(boolean showYear) {
        this.showYear = showYear;
    }

    public LoveDayModel(long startTime) {
        this.startTime = startTime;
        startDay0 = true;
        show100Day = true;
        showYear = true;
    }

    public long getStartTime() {
        return startTime;
    }

    public boolean isStartDay0() {
        return startDay0;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public void setStartDay0(boolean startDay0) {
        this.startDay0 = startDay0;
    }
}
