package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.BuildConfig;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background.BackgroundAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.ActivityMainBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.edit_text.EditTextAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.event.AddEventAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog.EventDialog;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog.EventDialogListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.MainDiaryFr;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.DetailDiaryAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.main_love.MainLoveFrm;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog.MainLoveDialog;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog.MainLoveDialogListener;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.render_list.RenderListFrm;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.setting.SettingFrm;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.EventModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.TimeModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.take_image.TakeImageAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.BitmapLoader;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.FileUtils;
import com.view.AppTextView;
import com.view.FontCache;
import com.view.Fonts;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AbsActivity<ActivityMainBinding, MainViewModel> {
    public static final int REQUEST_BACKGROUND = 101;
    private static final int REQUEST_ADD_EVENT = 102;
    private static final int REQUEST_EDIT_EVENT = 103;
    private static final int REQUEST_TOP_TITLE = 104;
    private static final int REQUEST_BOTTOM_TITLE = 105;
    private static final int REQUEST_NAME_USER_A = 106;
    private static final int REQUEST_NAME_USER_B = 107;
    private static final int REQUEST_AVATAR_USER_A = 108;
    private static final int REQUEST_AVATAR_USER_B = 109;

    public static Intent getIntent(@NonNull Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Class<MainViewModel> getViewModelClass() {
        return MainViewModel.class;
    }

    private RenderListFrm renderListFrm;
    private MainLoveFrm mainLoveFrm;
    private MainDiaryFr mainDiaryFr;
    private SettingFrm settingFrm;

    private MainPagerAdapter pagerAdapter;
    private MainLoveDialog mainLoveDialog;
    private EventDialog eventDialog;

    private MainListener mainListener = new MainListener() {

        @Override
        public void addEventDay() {
            navigateAddEvent();
        }

        @Override
        public void showMainLoveDialog() {
            showMainloveDialog();
        }

        @Override
        public void showEventDialog(final TimeModel timeModel) {
            eventDialog = new EventDialog(MainActivity.this);
            eventDialog.setListener(new EventDialogListener() {
                @Override
                public void onEditEvent() {
                    navigateEditEvent(timeModel);
                }

                @Override
                public void onDeleteEvent() {
                    viewModel.onRemoveEvent(timeModel.getWhatId());
                }
            });
            eventDialog.show();
        }

        @Override
        public void onChangeName(boolean owner, String content) {
            if (owner) {
                navigateEditText(REQUEST_NAME_USER_A, EditTextAct.EditType.USER_NAME_A, content);
            } else {
                navigateEditText(REQUEST_NAME_USER_B, EditTextAct.EditType.USER_NAME_B, content);
            }
        }

        @Override
        public void onChangeAvatar(boolean owner) {
            navigateChangeAvatar(owner);
        }

        @Override
        public void onRemoveAvatar(boolean owner) {
            onRemoveAvatarUserModel(owner);
        }

        @Override
        public void navigateBackground() {
            navigateChangeBackground();
        }

        @Override
        public void onStart0Changed(boolean b) {
            viewModel.onStart0Changed(b);
        }

        @Override
        public void onShowYearChanged(boolean b) {
            viewModel.onShowYearChanged(b);
        }

        @Override
        public void onShow100Day(boolean b) {
            viewModel.onShow100Day(b);
        }

        @Override
        public void onArlertEventChanged(boolean b) {
            viewModel.onArlertEventChanged(b);
        }

        @Override
        public void onShowInStatusBar(boolean b) {
            viewModel.onShowInStatusBar(b);
            LoveDayApp.getInstance().refreshNotifiStatusBar();
        }

        @Override
        public void setBackGroundColor(int selectedColor) {
            viewModel.setBackgroundColor(selectedColor);
        }

        @Override
        public void resetAllData() {
            LoveDayApp.getInstance().resetAllData();
        }
    };

    private void navigateEditEvent(TimeModel timeModel) {
        startActivityForResult(AddEventAct.getIntent(this, timeModel.getWhatId()),
                REQUEST_EDIT_EVENT);
    }

    private MainLoveDialogListener mainLoveDialogListener = new MainLoveDialogListener() {
        @Override
        public void onChangeDate() {
            LoveDayModel loveModel = viewModel.getCurrentLoveModel();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(loveModel.getStartTime());
            showDialogDatePicker(calendar);
        }

        @Override
        public void onChangeTopTitle() {
            navigateEditText(REQUEST_TOP_TITLE, EditTextAct.EditType.TOP_TITLE, viewModel.getTopTitle());
        }

        @Override
        public void onChangeBottomTitle() {
            navigateEditText(REQUEST_BOTTOM_TITLE, EditTextAct.EditType.BOT_TITLE, viewModel.getBottomTitle());
        }

        @Override
        public void onChangeBackground() {
            navigateChangeBackground();
        }

        @Override
        public void onShareScreenShort() {
            try {
                FileUtils.externalPermision(MainActivity.this, new FileUtils.PerListener() {
                    @Override
                    public void onGranted() {
                        takeScreenShotAndShare();
                    }

                    @Override
                    public void onDeny() {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

//    public void shareMore(Context context, Bitmap bitmap) {
//        Intent share = new Intent(Intent.ACTION_SEND);
//        share.setType("image/jpeg");
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "img_share.jpg");
//        try {
//            f.createNewFile();
//            FileOutputStream fo = new FileOutputStream(f);
//            fo.write(bytes.toByteArray());
//
//            Uri photoURI = FileProvider.getUriForFile(context,
//                    BuildConfig.APPLICATION_ID + ".provider", f);
//            share.putExtra("android.intent.extra.STREAM", photoURI);
//            context.startActivity(Intent.createChooser(share, "Share this photo"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void takeScreenShotAndShare() {
        try {
            Bitmap bitmap = BitmapLoader.loadBitmapFromView(binding.layoutMain);
            if (bitmap != null) {
                BitmapLoader.shareMore2(MainActivity.this, bitmap);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
//        View view = binding.viewPager;
//        view.getRootView();
//        String state = Environment.getExternalStorageState();
//        if (Environment.MEDIA_MOUNTED.equals(state)) {
//            File picDir = new File(Environment.getExternalStorageState() + "/myPic");
//            if (!picDir.exists()) {
//                picDir.mkdir();
//            }
//            view.setDrawingCacheEnabled(true);
//            view.buildDrawingCache(true);
//            Bitmap bitmap = view.getDrawingCache();
//            BitmapLoader.shareMore2(MainActivity.this, bitmap);
//            shareMore(this, bitmap);
//        } else {
//            //Error
//        }
    }

    private void navigateEditText(int request, @NonNull EditTextAct.EditType editType, String content) {
        startActivityForResult(EditTextAct.getIntent(this, editType, content), request);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            updateLayoutColor(position);
            updateAnimationProgress(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void updateAnimationProgress(int position) {
        if (position == 1 && mainLoveFrm != null) {
            mainLoveFrm.refreshAnimation();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_BACKGROUND:
                    initBackground();
                    break;
                case REQUEST_ADD_EVENT:
                case REQUEST_EDIT_EVENT:
                    if (data != null) {
                        EventModel model = data.getParcelableExtra(EventModel.class.getName());
                        if (model != null) {
                            onAddEventModel(model);
                        }
                    }
                    break;

                case REQUEST_TOP_TITLE:
                    if (data != null) {
                        String content = data.getStringExtra(String.class.getName());
                        if (content != null) {
                            onUpdateTopTitle(content);
                        }
                    }
                    break;
                case REQUEST_BOTTOM_TITLE:
                    if (data != null) {
                        String content = data.getStringExtra(String.class.getName());
                        if (content != null) {
                            onUpdateBottomTitle(content);
                        }
                    }
                    break;
                case REQUEST_NAME_USER_A:
                    if (data != null) {
                        String content = data.getStringExtra(String.class.getName());
                        if (content != null) {
                            onUpdateNameUserA(content);
                        }
                    }
                    break;
                case REQUEST_NAME_USER_B:
                    if (data != null) {
                        String content = data.getStringExtra(String.class.getName());
                        if (content != null) {
                            onUpdateNameUserB(content);
                        }
                    }
                    break;
                case REQUEST_AVATAR_USER_A:
                    if (data != null) {
                        Uri content = data.getParcelableExtra(TakeImageAct.RESULT_IMAGE);
                        if (content != null) {
                            onUpdateAvatarUserA(content.toString());
                        }
                    }
                    break;
                case REQUEST_AVATAR_USER_B:
                    if (data != null) {
                        Uri content = data.getParcelableExtra(TakeImageAct.RESULT_IMAGE);
                        if (content != null) {
                            onUpdateAvatarUserB(content.toString());
                        }
                    }
                    break;
            }
        }
    }

    private void onRemoveAvatarUserModel(boolean owner) {
        viewModel.onRemoveAvatarUserModel(owner);
        if (mainLoveFrm != null) mainLoveFrm.refreshUserModel();
    }

    private void onUpdateAvatarUserB(String content) {
        viewModel.onUpdateAvatarUser(content, false);
        if (mainLoveFrm != null) mainLoveFrm.refreshUserModel();
    }

    private void onUpdateAvatarUserA(String content) {
        viewModel.onUpdateAvatarUser(content, true);
        if (mainLoveFrm != null) mainLoveFrm.refreshUserModel();
    }

    private void onUpdateNameUserA(@NonNull String content) {
        viewModel.onUpdateNameUser(content, true);
        if (mainLoveFrm != null) mainLoveFrm.refreshUserModel();
    }

    private void onUpdateNameUserB(@NonNull String content) {
        viewModel.onUpdateNameUser(content, false);
        if (mainLoveFrm != null) mainLoveFrm.refreshUserModel();
    }

    private void onUpdateTopTitle(String content) {
        viewModel.saveTopTitle(content);
        if (mainLoveFrm != null) mainLoveFrm.updateLayoutTitle();
    }

    private void onUpdateBottomTitle(String content) {
        viewModel.saveBottomTitle(content);
        if (mainLoveFrm != null) mainLoveFrm.updateLayoutTitle();
    }

    @Override
    protected void initView() {
        initBackground();
        initViewPager();
    }

    private void initBackground() {
        Glide.with(this)
                .load(viewModel.getCurrentBackground())
                .into(binding.imageBg);
    }

    private void initViewPager() {

        renderListFrm = new RenderListFrm(mainListener);
        mainLoveFrm = new MainLoveFrm(mainListener);
        mainDiaryFr = new MainDiaryFr();
        settingFrm = new SettingFrm(mainListener);

        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(renderListFrm, "");
        pagerAdapter.addFragment(mainLoveFrm, getString(R.string.main_love_main_love_title));
        pagerAdapter.addFragment(mainDiaryFr, getString(R.string.main_love_diary_title));
        pagerAdapter.addFragment(settingFrm, "");

        binding.viewPager.setAdapter(pagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        setTablayout();

        binding.viewPager.addOnPageChangeListener(pageChangeListener);
        binding.viewPager.setCurrentItem(1);
    }

    private void setTablayout() {
        binding.tabLayout.setTabMode(TabLayout.MODE_AUTO);
        for (int i = 0; i < 4; i++) {
            TabLayout.Tab tab = binding.tabLayout.getTabAt(i);
            if (tab != null) {
                switch (i) {
                    case 0:
                        tab.setIcon(R.drawable.ic_list);
                        break;
                    case 1:
                        createTabTextLayout(tab, getString(R.string.main_love_main_love_title));
                        break;
                    case 2:
                        createTabTextLayout(tab, getString(R.string.main_love_diary_title));
                        break;
                    case 3:
                        tab.setIcon(R.drawable.ic_settings);
                        break;
                }
            }
        }
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        tab.setIcon(R.drawable.ic_list_active);
                        break;
                    case 1:
                    case 2:
                        View view = tab.getCustomView();
                        if (view instanceof TextView) {
                            view.setAlpha(1f);
                        }
                        break;
                    case 3:
                        tab.setIcon(R.drawable.ic_settings_active);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        tab.setIcon(R.drawable.ic_list);
                        break;
                    case 1:
                    case 2:
                        View view = tab.getCustomView();
                        if (view instanceof TextView) {
                            view.setAlpha(0.6f);
                        }
                        break;
                    case 3:
                        tab.setIcon(R.drawable.ic_settings);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void createTabTextLayout(TabLayout.Tab tab, String title) {
        AppTextView appTextView = new AppTextView(this);
        appTextView.setGravity(Gravity.CENTER);
        appTextView.setText(title);
        appTextView.setTextColor(Color.WHITE);
        appTextView.setTextSize(getResources().getDimensionPixelSize(R.dimen.tab_layout_text_size));
        appTextView.setTypeface(FontCache.loadFont(Fonts.BaseFiolexGirl, this));
        appTextView.setAlpha(0.6f);
        tab.setCustomView(appTextView);
    }

    private void updateLayoutColor(int position) {
        if (position == 1) {
            binding.layoutContainer.setBackgroundColor(getColorId(R.color.colorBg10));
        } else {
            binding.layoutContainer.setBackgroundColor(getColorId(R.color.colorBg20));
        }
    }


    private void showMainloveDialog() {
        if (mainLoveDialog == null) {
            mainLoveDialog = new MainLoveDialog(this);
            mainLoveDialog.setListener(mainLoveDialogListener);
        }
        mainLoveDialog.show();
    }


    private DatePickerDialog.OnDateSetListener datePickerListener =
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    viewModel.updateTimeLoveModel(DateUtils.formatDay(myCalendar));
                }
            };

    private void showDialogDatePicker(Calendar myCalendar) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                datePickerListener,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar maxCalendar = Calendar.getInstance();
        Calendar minCalendar = Calendar.getInstance();

        minCalendar.add(Calendar.YEAR, -100);

        datePickerDialog.getDatePicker().setMaxDate(maxCalendar.getTimeInMillis());
        datePickerDialog.getDatePicker().setMinDate(minCalendar.getTimeInMillis());
        datePickerDialog.show();
    }

    private void onAddEventModel(@NonNull EventModel model) {
        viewModel.addEventModel(model);
    }


    private void navigateAddEvent() {
        startActivityForResult(AddEventAct.getIntent(this), REQUEST_ADD_EVENT);
    }

    private void navigateChangeBackground() {
        startActivityForResult(BackgroundAct.getIntent(this), REQUEST_BACKGROUND);
    }

    private void navigateChangeAvatar(boolean owner) {
        int request;
        if (owner) {
            request = REQUEST_AVATAR_USER_A;
        } else {
            request = REQUEST_AVATAR_USER_B;
        }
        FileUtils.externalPermision(this, new FileUtils.PerListener() {
            @Override
            public void onGranted() {
                startActivityForResult(TakeImageAct.getIntent(MainActivity.this), request);
            }

            @Override
            public void onDeny() {
                Toast.makeText(MainActivity.this, getString(R.string.permission_on_deny),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
