package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.respository;


import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.model.DiaryEntity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

import java.util.List;

public interface DiaryRepository {

    LiveData<List<DiaryTable>> getListDiary();

    void insertDiaryEntity(DiaryEntity diaryEntity);

    LiveData<DiaryTable> getDiaryTable(@NonNull String id);

    LiveData<List<DiaryContentTable>> getListDiaryContent(@NonNull String id);

    void deleteDiary(String id);
}

