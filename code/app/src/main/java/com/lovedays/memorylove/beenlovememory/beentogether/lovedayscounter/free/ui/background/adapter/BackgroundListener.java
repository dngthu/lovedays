package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background.adapter;

public interface BackgroundListener {

    public void onSelectBackground(String path);

    public void onSelectImageFromGallery();
}
