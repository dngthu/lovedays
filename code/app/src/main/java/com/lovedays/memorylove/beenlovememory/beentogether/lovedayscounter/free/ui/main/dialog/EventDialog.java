package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.EventDialogBinding;

public class EventDialog extends BottomSheetDialog {
    private EventDialogListener listener;
    private EventDialogBinding binding;

    public EventDialog(@NonNull Context context) {
        super(context);
        initView();
    }

    public EventDialog(@NonNull Context context, int theme) {
        super(context, theme);
        initView();
    }

    public void setListener(EventDialogListener listener) {
        this.listener = listener;
    }

    private void initView() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.event_dialog,
                null, false);
        setContentView(binding.getRoot());
        bindAction();
    }

    private void bindAction() {
        binding.editEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onEditEvent();
                dismiss();
            }
        });

        binding.deleteEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onDeleteEvent();
                dismiss();
            }
        });
    }
}
