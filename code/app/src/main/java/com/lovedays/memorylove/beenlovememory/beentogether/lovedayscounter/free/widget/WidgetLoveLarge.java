package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.RemoteViews;


import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.AppWidgetTarget;
import com.bumptech.glide.request.transition.Transition;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.Const;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayManager;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.UserLoveModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.start.StartAppAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;

public class WidgetLoveLarge extends AppWidgetProvider {

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent != null && TextUtils.equals(intent.getAction(), Const.ACTION_UPDATE_WIDGET)) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(LoveDayApp.getInstance());
            int[] ids = appWidgetManager
                    .getAppWidgetIds(new ComponentName(LoveDayApp.getInstance(), WidgetLoveLarge.class));
            onUpdate(context, appWidgetManager, ids);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            Intent intent = StartAppAct.getIntent(context);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.layout_widget_love_large);

            views.setOnClickPendingIntent(R.id.btn_click, pendingIntent);

            LoveDayManager manager = LoveDayManager.getManager(LoveDayApp.getInstance().getSharedPre());
            loadImageBg(context, appWidgetIds, views, R.id.image_bg, manager.getCurrentBackground());

            LoveDayModel model = manager.getCurrentLoveModel();
            if (model != null) {
                int count = DateUtils.getCountDay(model.isStartDay0(),
                        model.getStartTime(),
                        DateUtils.toDay());
                views.setTextViewText(R.id.count_day_tv, String.valueOf(count));
            }

            String topTitle1 = manager.getTopTitle();
            String bottomTitle1 = manager.getBottomTitle();

            if (TextUtils.isEmpty(topTitle1))
                topTitle1 = context.getString(R.string.main_love_top_title_default);
            if (TextUtils.isEmpty(bottomTitle1))
                bottomTitle1 = context.getString(R.string.main_love_bottom_title_default);

            views.setTextViewText(R.id.top_title, topTitle1);
            views.setTextViewText(R.id.bottom_title, bottomTitle1);

            UserLoveModel userModel = manager.getUserLoveModel();
            loadImageAvatar(context, appWidgetIds, views, R.id.avatar_user_a, userModel.getAvatarA());
            loadImageAvatar(context, appWidgetIds, views, R.id.avatar_user_b, userModel.getAvatarB());
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    private void loadImageBg(Context context, int[] appWidgetIds, RemoteViews views, int id, String url) {
        loadImage(context, appWidgetIds, views, id, url, 500, 500, false);
    }

    private void loadImageAvatar(Context context, int[] appWidgetIds, RemoteViews views, int id, String url) {
        loadImage(context, appWidgetIds, views, id, url, 120, 120, true);
    }

    private void loadImage(Context context, int[] appWidgetIds, RemoteViews views,
                           int id, String url,
                           int width,
                           int height,
                           boolean circle) {
        AppWidgetTarget appWidgetTarget = new AppWidgetTarget(context, id, views, appWidgetIds) {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                super.onResourceReady(resource, transition);
            }
        };

        RequestBuilder<Bitmap> value = Glide.with(context.getApplicationContext())
                .asBitmap()
                .apply(new RequestOptions().override(width, height));
        if (circle) {
            value.apply(RequestOptions.circleCropTransform());
        }
        if (TextUtils.isEmpty(url)&& circle){
            value.load(R.drawable.ic_avatar).into(appWidgetTarget);
        }else {
            value.load(url).into(appWidgetTarget);
        }
    }
}
