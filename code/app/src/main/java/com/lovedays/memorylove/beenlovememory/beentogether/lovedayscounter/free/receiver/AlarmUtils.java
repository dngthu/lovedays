package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

public class AlarmUtils {
    private static final int HOUR_SHOW_NOTIFICATION = 9;
    private static final int COUNT_DAY_SHOW_NOTIFICATION = 3;

    // show notification vao 9H00 nay hom sau
    public static void create(Context context) {
        try {
            for (int i = 1; i <= COUNT_DAY_SHOW_NOTIFICATION; i++) {

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                Intent intent = OpenPhoneService.getIntent(context);
                PendingIntent pendingIntent =
                        PendingIntent.getService(context, 101,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, i);
                calendar.set(Calendar.HOUR, HOUR_SHOW_NOTIFICATION);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                if (alarmManager != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    } else {
                        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
