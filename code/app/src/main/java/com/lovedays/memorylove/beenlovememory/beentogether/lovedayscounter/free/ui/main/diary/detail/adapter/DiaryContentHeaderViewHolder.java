package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.DiaryContentHeaderItemBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;

public class DiaryContentHeaderViewHolder extends RecyclerView.ViewHolder {

    @NonNull
    private DiaryContentHeaderItemBinding binding;

    @NonNull
    private DiaryContentListener diaryContentListener;

    public DiaryContentHeaderViewHolder(@NonNull DiaryContentHeaderItemBinding binding,
                                        @NonNull DiaryContentListener diaryContentListener) {
        super(binding.getRoot());
        this.binding = binding;
        this.diaryContentListener = diaryContentListener;
    }

    public static DiaryContentHeaderViewHolder create(ViewGroup parent, DiaryContentListener diaryContentListener) {
        return new DiaryContentHeaderViewHolder(DiaryContentHeaderItemBinding
                .inflate(LayoutInflater.from(parent.getContext()),
                parent, false),
                diaryContentListener);
    }

    public void bind(){
        diaryContentListener.observeDiary(diaryTable -> {
            if (diaryTable!=null){
                loadImageCover(diaryTable.getCover());
                binding.titleTv.setText(diaryTable.getTitle());
                binding.timeTv.setText(DateUtils.getDateString(diaryTable.getTimeCreate()));
            }
        });
    }

    private void loadImageCover(String cover) {
        if (cover!=null && !cover.isEmpty()){
            Glide.with(itemView.getContext())
                    .load(cover)
                    .apply(new RequestOptions().placeholder(R.color.colorTransparent))
                    .apply(new RequestOptions().error(R.color.colorTransparent))
                    .into(binding.coverV);
            binding.coverV.setVisibility(View.VISIBLE);
            binding.timeTv.setTextColor(Color.WHITE);
            binding.titleTv.setTextColor(Color.WHITE);
            binding.titleLayout.setBackgroundResource(R.drawable.bg_diary_content_header_title);
        }else {
            binding.coverV.setVisibility(View.GONE);
            binding.timeTv.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.colorTextBlack));
            binding.titleTv.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.colorTextBlack));
            binding.titleLayout.setBackgroundColor(Color.TRANSPARENT);
        }
    }
}