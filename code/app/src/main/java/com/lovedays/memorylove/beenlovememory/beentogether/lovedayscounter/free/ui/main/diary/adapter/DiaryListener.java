package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter;

import androidx.annotation.NonNull;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

public interface DiaryListener {

    void onCreateDiary();

    void detailDiary(@NonNull DiaryTable diaryTable);

}
