package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.CreateDiaryTextViewBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTextStyle;

public class CreateDiaryTextView extends FrameLayout {
    public CreateDiaryTextView(@NonNull Context context) {
        super(context);
        initView();
    }

    public CreateDiaryTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CreateDiaryTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private CreateDiaryTextViewBinding binding;

    private MutableLiveData<Boolean> activeBold = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> activeItalic = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> activeCenter = new MutableLiveData<>(false);

    private CreateDiaryListener createDiaryListener;

    public void setCreateDiaryListener(CreateDiaryListener createDiaryListener) {
        this.createDiaryListener = createDiaryListener;
        initHandleStyle();
    }

    private void initView() {
        binding = CreateDiaryTextViewBinding.inflate(LayoutInflater.from(getContext()));
        addView(binding.getRoot(), FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        initActions();
    }

    private void initActions() {
        binding.boldBtn.setOnClickListener(view -> {
            if (activeBold.getValue() != null && activeBold.getValue()) {
                activeBold.setValue(false);
            } else {
                activeBold.setValue(true);
            }
        });
        binding.italicBtn.setOnClickListener(view -> {
            if (activeItalic.getValue() != null && activeItalic.getValue()) {
                activeItalic.setValue(false);
            } else {
                activeItalic.setValue(true);
            }
        });
        binding.centerBtn.setOnClickListener(view -> {
            if (activeCenter.getValue() != null && activeCenter.getValue()) {
                activeCenter.setValue(false);
            } else {
                activeCenter.setValue(true);
            }
        });
        binding.removeBtn.setOnClickListener(view ->
                createDiaryListener.onRemoveText(CreateDiaryTextView.this));
    }

    private void initHandleStyle() {
        if (createDiaryListener != null) {
            activeBold.observe(createDiaryListener.getLifecycleOwner(), aBoolean -> {
                if (aBoolean!=null && aBoolean){
                    binding.boldBtn.setImageResource(R.drawable.bg_active_bold_style_item);
                }else {
                    binding.boldBtn.setImageResource(R.drawable.bg_bold_style_item);
                }
                updateStyle();
            });

            activeItalic.observe(createDiaryListener.getLifecycleOwner(), aBoolean -> {
                if (aBoolean!=null && aBoolean){
                    binding.italicBtn.setImageResource(R.drawable.bg_active_italic_style_item);
                }else {
                    binding.italicBtn.setImageResource(R.drawable.bg_italic_style_item);
                }
                updateStyle();
            });
            activeCenter.observe(createDiaryListener.getLifecycleOwner(), aBoolean -> {
                if (aBoolean!=null && aBoolean){
                    binding.centerBtn.setImageResource(R.drawable.bg_active_center_style_item);
                    binding.contentEdt.setGravity(Gravity.CENTER);
                }else {
                    binding.centerBtn.setImageResource(R.drawable.bg_center_style_item);
                    binding.contentEdt.setGravity(Gravity.NO_GRAVITY);
                }
            });
        }
    }

    private DiaryContentTable model;


    public void setModel(DiaryContentTable table) {
        this.model = table;
        binding.contentEdt.setText(table.getContent());
        activeCenter.setValue(table.isTextCenter());
        switch (table.getTextStyle()){
            case DiaryContentTextStyle.BOLD_ITALIC:
                activeBold.setValue(true);
                activeItalic.setValue(true);
                break;
            case DiaryContentTextStyle.BOLD:
                activeBold.setValue(true);
                activeItalic.setValue(false);
                break;
            case DiaryContentTextStyle.ITALIC:
                activeBold.setValue(false);
                activeItalic.setValue(true);
                break;
            default:
                activeBold.setValue(false);
                activeItalic.setValue(false);
                break;
        }
    }

    @SuppressLint("WrongConstant")
    private void updateStyle() {
        boolean bold = activeBold.getValue()!=null ? activeBold.getValue() : false;
        boolean italic = activeItalic.getValue()!=null ? activeItalic.getValue() : false;
        if (bold && italic) {
            binding.contentEdt.setTypeface(getTypeface(), Typeface.BOLD | Typeface.ITALIC);
        } else if (bold) {
            binding.contentEdt.setTypeface(getTypeface(), Typeface.BOLD);
        } else if (italic) {
            binding.contentEdt.setTypeface(getTypeface(), Typeface.ITALIC);
        } else {
            binding.contentEdt.setTypeface(getTypeface(), Typeface.NORMAL);
        }
    }

    @NonNull
    private Typeface getTypeface() {
        return Typeface.create(binding.contentEdt.getTypeface(), Typeface.NORMAL);
    }

    @Nullable
    public DiaryContentTable getContent(){
        Editable text = binding.contentEdt.getText();
        String content = text!=null? text.toString().trim(): "";
        boolean bold = activeBold.getValue()!=null ? activeBold.getValue() : false;
        boolean italic = activeItalic.getValue()!=null ? activeItalic.getValue() : false;
        int textStyle ;
        if (bold && italic) {
            textStyle = DiaryContentTextStyle.BOLD_ITALIC;
        } else if (bold) {
            textStyle = DiaryContentTextStyle.BOLD;
        } else if (italic) {
            textStyle = DiaryContentTextStyle.ITALIC;
        } else {
            textStyle = DiaryContentTextStyle.NORMAL;
        }
        boolean center = activeCenter.getValue()!=null ? activeCenter.getValue(): false;
        if (!content.isEmpty()){
            if (model!=null){
                model.updateText(content, textStyle, center);
                return model;
            }
            return new DiaryContentTable( content, textStyle,  center);
        }
        return null;
    }

}