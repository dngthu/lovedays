package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public abstract class AbsFragment<B extends ViewDataBinding, M extends AbsViewModel>
        extends Fragment {

    protected M viewModel;
    protected B binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        viewModel = new ViewModelProvider(this).get(getViewModelClass());
        initView();
        return binding.getRoot();
    }

    protected abstract int getLayoutId();

    protected abstract Class<M> getViewModelClass();

    protected abstract void initView();
}
