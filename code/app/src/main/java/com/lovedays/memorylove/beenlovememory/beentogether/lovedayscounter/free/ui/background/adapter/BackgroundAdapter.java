package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background.adapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BackgroundAdapter extends RecyclerView.Adapter<BackgroundViewHolder> {
    private static final String HEADER = "HEADER";
    private List<String> listBg;
    private BackgroundListener listener;

    public BackgroundAdapter(List<String> listBg, BackgroundListener listener) {
        this.listBg = listBg;
        this.listBg.add(0, HEADER);
        this.listener = listener;
    }

    @NonNull
    @Override
    public BackgroundViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return BackgroundViewHolder.create(parent, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BackgroundViewHolder holder, int position) {
        String content = listBg.get(position);
        if (TextUtils.equals(content, HEADER)) {
            holder.onBindHeader();
        } else {
            holder.onBind(content);
        }
    }

    @Override
    public int getItemCount() {
        return listBg.size();
    }
}
