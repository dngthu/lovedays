package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.respository;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.AppDatabase;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.dao.DiaryDao;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.model.DiaryEntity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

import java.util.List;

public class DiaryRepositoryImpl implements DiaryRepository {
    private static DiaryRepository repository;

    @NonNull
    public static DiaryRepository getInstance(@NonNull Context context){
        if (repository==null){
            repository = new DiaryRepositoryImpl(context);
        }
        return repository;
    }

    private DiaryDao diaryDao;

    private DiaryRepositoryImpl(@NonNull Context context) {
        diaryDao = AppDatabase.getInstance(context).getNoteDao();
    }

    @Override
    public LiveData<List<DiaryTable>> getListDiary() {
        return diaryDao.getDiaryTables();
    }

    @Override
    public void insertDiaryEntity(DiaryEntity diaryEntity) {
        diaryDao.insert(diaryEntity);
    }

    @Override
    public LiveData<DiaryTable> getDiaryTable(@NonNull String id) {
        return diaryDao.getDiaryTable(id);
    }

    @Override
    public LiveData<List<DiaryContentTable>> getListDiaryContent(@NonNull String id) {
        return diaryDao.getListDiaryContent(id);
    }

    @Override
    public void deleteDiary(String id) {
        diaryDao.removeContent(id);
        diaryDao.deleteDiary(id);
    }
}
