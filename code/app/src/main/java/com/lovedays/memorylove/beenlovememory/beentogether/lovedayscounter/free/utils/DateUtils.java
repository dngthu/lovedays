package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static final int ONE_DAY = 24 * 60 * 60 * 1000;
    public static final String DATE_FORMAT_DD_MM_YYYY = "dd-MM-yyyy";
    public static final String DATE_FORMAT_MMMM_DD_YYYY = "d MMM, yyyy";
    public static final String DATE_FORMAT_MMMM_DD_YYYY_HH_MM = "d MMM, yyyy, HH, mm";

    public static String getDateString(long longTime) {
        return getDateString(longTime, DATE_FORMAT_MMMM_DD_YYYY);
    }

    public static String getDateString(long longTime, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format,
                Locale.getDefault());
        Date formatted = new Date(longTime);
        return formatter.format(formatted);
    }

    @SuppressLint("SimpleDateFormat")
    public static int getCountDay(boolean start0, long checkIn, long checkOut) {
        Calendar calendarCheckIn = Calendar.getInstance();
        calendarCheckIn.setTimeInMillis(checkIn);
        Calendar calendarCheckout = Calendar.getInstance();
        calendarCheckout.setTimeInMillis(checkOut);
//        formatDay(calendarCheckIn);
//        formatDay(calendarCheckout);
        long timeOut = calendarCheckout.getTimeInMillis();
        long timeIn = calendarCheckIn.getTimeInMillis();
        return (int) ((timeOut - timeIn) / ONE_DAY) + (start0 ? 0 : 1);
    }

    @SuppressLint("SimpleDateFormat")
    public static int getCountDay2(boolean start0, long checkIn, long checkOut) {
        Calendar calendarCheckIn = Calendar.getInstance();
        calendarCheckIn.setTimeInMillis(checkIn);
        Calendar calendarCheckout = Calendar.getInstance();
        calendarCheckout.setTimeInMillis(checkOut);
        formatDay(calendarCheckIn);
        formatDay(calendarCheckout);
        long timeOut = calendarCheckout.getTimeInMillis();
        long timeIn = calendarCheckIn.getTimeInMillis();
        return (int) ((timeOut - timeIn) / ONE_DAY);
    }

    @SuppressLint("SimpleDateFormat")
    public static int getCountDayBetween(long checkIn, long checkOut) {
        try {
            Calendar calendarCheckIn = Calendar.getInstance();
            calendarCheckIn.setTime(new Date(checkIn));
            Calendar calendarCheckout = Calendar.getInstance();
            calendarCheckout.setTime(new Date(checkOut));
            if (checkOut > checkIn) {
                return getCountDayBetween(calendarCheckIn, calendarCheckout);
            } else {
                return -getCountDayBetween(calendarCheckout, calendarCheckIn);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @SuppressLint("SimpleDateFormat")
    public static int getCountDayBetween(@NonNull Calendar checkIn, @NonNull Calendar checkOut) {
        try {
            int count = 0;
            if (checkIn.get(Calendar.YEAR) == checkOut.get(Calendar.YEAR)) {
                return checkOut.get(Calendar.DAY_OF_YEAR) - checkIn.get(Calendar.DAY_OF_YEAR);
            }
            boolean first = true;
            while (checkIn.get(Calendar.YEAR) < checkOut.get(Calendar.YEAR)) {
                count = checkIn.getActualMaximum(Calendar.DAY_OF_YEAR);
                if (first) {
                    count = count - checkIn.get(Calendar.DAY_OF_YEAR);
                    first = false;
                }
                checkIn.add(Calendar.YEAR, 1);
            }
            count = count + checkOut.get(Calendar.DAY_OF_YEAR);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long formatDay(@NonNull Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static long toDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }
}
