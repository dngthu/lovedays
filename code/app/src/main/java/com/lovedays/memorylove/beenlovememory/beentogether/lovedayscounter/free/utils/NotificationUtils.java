package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.NotificationTarget;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayManager;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.UserLoveModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.start.StartAppAct;

import java.util.concurrent.ExecutionException;

public class NotificationUtils {

    private static final String CHANNEL_ID_STATUS = "CHANNEL_ID_STATUS";
    private static final String CHANNEL_NAME_STATUS = "CHANNEL_NAME_STATUS";
    private static final int NOTIFICATION_STATUS_ID = 101;


    private static final String CHANNEL_ID_ARLERT = "CHANNEL_ID_ARLERT";
    private static final String CHANNEL_NAME_ARLERT = "CHANNEL_NAME_ARLERT";
    private static final int NOTIFICATION_ARLERT_ID = 102;


    public static void clearNotificationStatusBar(@NonNull Context context) {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mNotificationManager.deleteNotificationChannel(CHANNEL_ID_STATUS);
            } else {
                mNotificationManager.cancel(NOTIFICATION_STATUS_ID);
            }
        }
    }

    public static void buildNotificationStatusBar(@NonNull Context context) {
        buildNotification(context, CHANNEL_ID_STATUS,
                CHANNEL_NAME_STATUS,
                NOTIFICATION_STATUS_ID,
                false);
    }

    public static void buildNotification(@NonNull Context context) {
        buildNotification(context,
                CHANNEL_ID_ARLERT,
                CHANNEL_NAME_ARLERT,
                NOTIFICATION_ARLERT_ID,
                true);
    }

    private static void buildNotification(@NonNull Context context,
                                          String chanelId,
                                          String chanelName,
                                          int notificationId,
                                          boolean clear) {

        try {
            LoveDayManager manager = LoveDayManager.getManager(LoveDayApp.getInstance().getSharedPre());
            if (manager.getCurrentLoveModel() != null) {

                Intent intent = StartAppAct.getIntent(context);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                        intent, 0);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, chanelId);

                builder.setSmallIcon(R.drawable.ic_heart)
                        .setOngoing(!clear)
                        .setSound(null, Notification.STREAM_DEFAULT)
                        .setPriority(Notification.PRIORITY_MIN)
                        .setCustomBigContentView(null);

                RemoteViews notificationLayout = new RemoteViews(context.getPackageName(),
                        R.layout.layout_notify_love_medium);

                builder.setCustomContentView(notificationLayout);

                NotificationManager mNotificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                if (mNotificationManager != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel channel = new NotificationChannel(
                                chanelId,
                                chanelName,
                                NotificationManager.IMPORTANCE_DEFAULT);
                        mNotificationManager.createNotificationChannel(channel);
                        builder.setChannelId(chanelId);
                    }
                    Notification notification = builder.build();
                    if (!clear) {
                        notification.flags = Notification.FLAG_NO_CLEAR;
                    }
                    mNotificationManager.notify(notificationId, notification);

                    updateLayoutMedium(context, notificationLayout,
                            notification,
                            notificationId, pendingIntent, manager);
                }
            } else {
                clearNotificationStatusBar(context);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void updateLayoutMedium(@NonNull Context context,
                                           @NonNull RemoteViews views,
                                           final Notification notification,
                                           final int notificationId,
                                           @NonNull PendingIntent pendingIntent,
                                           @NonNull LoveDayManager manager) {
        try {
            views.setOnClickPendingIntent(R.id.btn_click, pendingIntent);
            loadImageBg(context, views, notification, notificationId, R.id.image_bg, manager.getCurrentBackground());
            LoveDayModel model = manager.getCurrentLoveModel();
            if (model != null) {
                int count = DateUtils.getCountDay(model.isStartDay0(),
                        model.getStartTime(),
                        DateUtils.toDay());
                views.setTextViewText(R.id.count_day_tv, String.valueOf(count));
            }

//            String topTitle1 = manager.getTopTitle();
//            String bottomTitle1 = manager.getBottomTitle();
//            if (TextUtils.isEmpty(topTitle1))
//                topTitle1 = context.getString(R.string.main_love_top_title_default);
//            if (TextUtils.isEmpty(bottomTitle1))
//                bottomTitle1 = context.getString(R.string.main_love_bottom_title_default);
//
//            views.setTextViewText(R.id.top_title, topTitle1);
//            views.setTextViewText(R.id.bottom_title, bottomTitle1);

            UserLoveModel userModel = manager.getUserLoveModel();

            String nameA = userModel.getNameA();
            String nameB = userModel.getNameB();

            if (TextUtils.isEmpty(nameA))
                nameA = context.getString(R.string.user_love_view_name_a_default);
            views.setTextViewText(R.id.name_user_a, nameA);
            if (TextUtils.isEmpty(nameB))
                nameB = context.getString(R.string.user_love_view_name_b_default);
            views.setTextViewText(R.id.name_user_b, nameB);

            loadImageAvatar(context, views, notification, notificationId, R.id.avatar_user_a, userModel.getAvatarA());
            loadImageAvatar(context, views, notification, notificationId, R.id.avatar_user_b, userModel.getAvatarB());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadImageBg(Context context,
                                    RemoteViews views,
                                    final Notification notification,
                                    final int notificationId, int id, String url) {
        loadImage(context, views, notification, notificationId, id, url, 200, 200, false);
    }

    private static void loadImageAvatar(Context context, RemoteViews views,
                                        final Notification notification,
                                        final int notificationId, int id, String url) {
        loadImage(context, views, notification, notificationId, id, url, 120, 120, true);
    }

    private static void loadImage(final Context context,
                                  final RemoteViews views,
                                  final Notification notification,
                                  final int notificationId,
                                  final int id, final String url,
                                  final int width,
                                  final int height,
                                  final boolean circle) {


//        new AsyncImage(context, views, id, width, height, circle).execute(url);

        try {
            NotificationTarget simpleTarget = new NotificationTarget(context, id, views, notification, notificationId);

            RequestBuilder<Bitmap> value = Glide.with(context.getApplicationContext())
                    .asBitmap()
                    .apply(new RequestOptions().override(width, height));
            if (circle) {
                value.apply(RequestOptions.circleCropTransform())
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_avatar))
                        .apply(RequestOptions.errorOf(R.drawable.ic_avatar));
            }
            if (TextUtils.isEmpty(url) && circle) {
                value.load(R.drawable.ic_avatar).into(simpleTarget);
            } else {
                value.load(url).into(simpleTarget);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class AsyncImage extends AsyncTask<String, Void, Bitmap> {

        private RemoteViews remoteViews;
        private boolean avatar;
        private int viewid;
        private Context context;
        private int width;
        private int height;

        public AsyncImage(Context context, RemoteViews views, int viewid, int width, int height, boolean avatar) {
            this.context = context;
            this.remoteViews = views;
            this.viewid = viewid;
            this.avatar = avatar;
            this.width = width;
            this.height = height;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                String url = strings[0];
                RequestBuilder<Bitmap> value = Glide.with(context)
                        .asBitmap()
                        .apply(new RequestOptions().override(width, height));
                if (avatar) {
                    value.apply(RequestOptions.circleCropTransform())
                            .apply(RequestOptions.placeholderOf(R.drawable.ic_avatar))
                            .apply(RequestOptions.errorOf(R.drawable.ic_avatar));
                }
                value.load(BitmapLoader.loadBitmapUri(context, url));
//                value.load(url);

                Bitmap bitmap = value.submit().get();
                return bitmap;
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (remoteViews != null) {
                remoteViews.setImageViewBitmap(viewid, bitmap);
            }
        }
    }


}
