package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table;

public @interface DiaryContentType {
    int TEXT = 1;
    int IMAGE = 2;
}
