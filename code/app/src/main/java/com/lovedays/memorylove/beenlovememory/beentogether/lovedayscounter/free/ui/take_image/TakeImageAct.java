package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.take_image;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.background.BackgroundAct;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.TakeImageActBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.Strings;
import com.yalantis.ucrop.UCrop;

import java.io.File;

public class TakeImageAct extends AbsActivity<TakeImageActBinding, TakeImageViewModel> {

    private static final int REQUEST_IMAGE_GALLERY = 100;
    public static final String RESULT_IMAGE = "TakeImageAct-RESULT_IMAGE";
    private static final String TAKE_BACKGROUND = "TakeImageAct-TAKE_BACKGROUND";
    private static final String TAKE_RATIO = "TakeImageAct-TAKE_RATIO";

    public static Intent getIntent(@NonNull Context context) {
        return new Intent(context, TakeImageAct.class);
    }

    public static Intent getIntentFull(@NonNull Context context) {
        Intent intent = new Intent(context, TakeImageAct.class);
        intent.putExtra(TAKE_BACKGROUND, true);
        return intent;
    }

    public static Intent getIntentRatio(@NonNull Context context, float ratio) {
        Intent intent = new Intent(context, TakeImageAct.class);
        intent.putExtra(TAKE_RATIO, ratio);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.choose_image_act;
    }

    @Override
    protected Class<TakeImageViewModel> getViewModelClass() {
        return TakeImageViewModel.class;
    }

    @Override
    protected void initView() {
        viewModel.background = getIntent().getBooleanExtra(TAKE_BACKGROUND, false);
        viewModel.ratio = getIntent( ).getFloatExtra(TAKE_RATIO, 0f );

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_IMAGE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case REQUEST_IMAGE_GALLERY:
                    final Uri selectedUri = data.getData();
                    if (selectedUri != null) {
                        startCrop(selectedUri);
                    } else {
                        Toast.makeText(TakeImageAct.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case UCrop.REQUEST_CROP:
                    handleCropResult(data);
                    break;
            }
        } else if (requestCode == UCrop.RESULT_ERROR) {
            finish();
        } else {
            finish();
        }
    }

    private void handleCropResult(Intent data) {
        final Uri resultUri = UCrop.getOutput(data);
        if (resultUri != null) {
            Intent intent = new Intent();
            intent.putExtra(RESULT_IMAGE, resultUri);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "Image";

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += System.currentTimeMillis() + ".png";

        File fileResult = new File(getCacheDir(), destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(fileResult));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(this);

    }

    private UCrop basisConfig(@NonNull UCrop uCrop) {
        if (viewModel.background) {
            float ration = (float) LoveDayApp.HEIGHT_SCREEN / LoveDayApp.WIDTH_SCREEN;
            uCrop.withAspectRatio(1, ration);
        } else if (viewModel.ratio> 0){
            uCrop.withAspectRatio(viewModel.ratio, 1);
        }else {
            uCrop.withAspectRatio(1, 1);
        }
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(100);
        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(false);
        return uCrop.withOptions(options);
    }
}
