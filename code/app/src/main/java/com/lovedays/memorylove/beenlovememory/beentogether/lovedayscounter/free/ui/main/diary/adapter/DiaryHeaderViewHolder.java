package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.DiaryHeaderItemBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

public class DiaryHeaderViewHolder extends RecyclerView.ViewHolder {

    @NonNull
    private DiaryHeaderItemBinding binding;

    @NonNull
    private DiaryListener diaryListener;

    public DiaryHeaderViewHolder(@NonNull DiaryHeaderItemBinding binding,
                                 @NonNull DiaryListener diaryListener) {
        super(binding.getRoot());
        this.binding = binding;
        this.diaryListener = diaryListener;
    }

    public static DiaryHeaderViewHolder create(ViewGroup parent, DiaryListener diaryListener) {
        return new DiaryHeaderViewHolder(DiaryHeaderItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false),
                diaryListener);
    }

    public void bind(){
        binding.titleTv.setOnClickListener(view -> diaryListener.onCreateDiary());
    }
}