package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorChangedListener;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsFragment;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.SettingFrmBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.MainListener;

public class SettingFrm extends AbsFragment<SettingFrmBinding, SettingViewModel> {

    private MainListener mainListener;

    public SettingFrm(MainListener mainListener) {
        this.mainListener = mainListener;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.setting_frm;
    }

    @Override
    protected Class<SettingViewModel> getViewModelClass() {
        return SettingViewModel.class;
    }

    @Override
    protected void initView() {
        initViewSetting();
        bindAction();
    }

    private void initViewSetting() {
        LoveDayModel model = viewModel.getLoveDayModel();
        if (model != null) {
            binding.cbStart0.setChecked(model.isStartDay0());
            binding.cbShowYearDay.setChecked(model.isShowYear());
            binding.cbShow100Day.setChecked(model.isShow100Day());
        }
        binding.cbArlertEvent.setChecked(viewModel.isArlertEvent());
        binding.cbShowStatusBar.setChecked(viewModel.isShowInStatusBar());
    }

    private void bindAction() {

        binding.colorBgTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogColorPicker();
            }
        });

        binding.btnBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainListener.navigateBackground();
            }
        });

        binding.cbStart0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                compoundButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainListener.onStart0Changed(b);
                    }
                });
            }
        });

        binding.cbShowStatusBar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                compoundButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainListener.onShowInStatusBar(b);
                    }
                });
            }
        });

        binding.cbArlertEvent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                compoundButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainListener.onArlertEventChanged(b);
                    }
                });
            }
        });
        binding.cbShow100Day.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                compoundButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainListener.onShow100Day(b);
                    }
                });
            }
        });
        binding.cbShowYearDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                compoundButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainListener.onShowYearChanged(b);
                    }
                });
            }
        });
        binding.btnResetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.main_setting_arlert_reset_data)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mainListener.resetAllData();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                // Create the AlertDialog object and return it
                builder.create().show();
            }
        });
    }

    private void showDialogColorPicker() {

        ColorPickerDialogBuilder
                .with(getContext())
                .setTitle(R.string.color_dialog_title)
                .initialColor(Color.WHITE)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton(android.R.string.ok, new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        changeBackgroundColor(selectedColor);
                    }
                })
                .showColorEdit(false)
                .build()
                .show();
    }

    private void changeBackgroundColor(int selectedColor) {
        if (mainListener != null) mainListener.setBackGroundColor(selectedColor);
    }
}
