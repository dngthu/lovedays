package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.splash;

import android.content.Intent;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.SplashActBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.start.StartAppAct;

public class SplashActivity extends AbsActivity<SplashActBinding, SplashViewModel> {

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected boolean isTransparentStatusBar() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.splash_act;
    }

    @Override
    protected Class<SplashViewModel> getViewModelClass() {
        return SplashViewModel.class;
    }

    @Override
    protected void initView() {

        Glide.with(SplashActivity.this)
                .load(R.drawable.icon_app)
                .apply(RequestOptions.circleCropTransform())
                .into(binding.iconApp);

        IntentLauncher launcher = new IntentLauncher();
        launcher.start();
    }

    private class IntentLauncher extends Thread {
        /**
         * Sleep for some time and than start new activity.
         */
        @Override
        public void run() {
            try {
                // load truoc background


                // load truoc background
                Glide.with(SplashActivity.this)
                        .asBitmap()
                        .load(viewModel.getCurrentBackground())
                        .submit();
                Thread.sleep(800);
            } catch (Exception e) {

            }
            // Start main activity
            Intent intent = new Intent(SplashActivity.this, StartAppAct.class);
            SplashActivity.this.startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
