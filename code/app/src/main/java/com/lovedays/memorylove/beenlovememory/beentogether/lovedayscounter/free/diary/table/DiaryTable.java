package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity
public class DiaryTable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "diaId")
    private String id = UUID.randomUUID().toString();
    @ColumnInfo(name = "diaTimeCre")
    private long timeCreate ;
    @ColumnInfo(name = "diaTimeUpd")
    private long timeUpdate;
    @ColumnInfo(name = "diaTit")
    private String title;
    @ColumnInfo(name = "diaCover")
    private String cover;
    @ColumnInfo(name = "diaDes")
    private String desciption;

    public DiaryTable(@NonNull String title, String cover) {
        this.timeCreate = System.currentTimeMillis();
        this.timeUpdate = System.currentTimeMillis();
        this.title = title;
        this.cover = cover;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public void setTimeUpdate(long timeUpdate) {
        this.timeUpdate = timeUpdate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public long getTimeCreate() {
        return timeCreate;
    }

    public long getTimeUpdate() {
        return timeUpdate;
    }


    public String getTitle() {
        return title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
