package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table;

public @interface DiaryContentTextStyle {
    int NORMAL = 0;
    int BOLD = 1;
    int ITALIC = 2;
    int BOLD_ITALIC = 3;
}
