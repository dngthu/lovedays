package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.databinding.DataBindingUtil;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.CountLoveViewBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.SizeUtils;

public class CountLoveView extends FrameLayout {
    private static final int TIME = 3000;
    private CountLoveViewBinding binding;

    private LoveDayModel loveDayModel;
    private int maxCount = -1;

    public CountLoveView(Context context) {
        super(context);
        initView();
    }

    public CountLoveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CountLoveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.count_love_view,
                this,
                false);
        addView(binding.getRoot(), LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        initWaveView();
    }

    private void initWaveView() {
        binding.waveLoadingView.setWaterLevelRatio(0f);
        binding.waveLoadingView.setTopTitleSize(getResources()
                .getDimensionPixelSize(R.dimen.top_title_text_size));

        binding.waveLoadingView.setCenterTitleSize(getResources()
                .getDimensionPixelSize(R.dimen.center_title_text_size));

        binding.waveLoadingView.setBottomTitleSize(getResources()
                .getDimensionPixelSize(R.dimen.bottom_title_text_size));
    }

    public void setLoveDayModel(LoveDayModel loveDayModel) {
        this.loveDayModel = loveDayModel;
        updateLayout();
    }

    private void updateLayout() {
        if (loveDayModel != null) {
            int count = DateUtils.getCountDay(loveDayModel.isStartDay0(),
                    loveDayModel.getStartTime(),
                    DateUtils.toDay());
            binding.countText.setText(String.valueOf(count));
            binding.waveLoadingView.setCenterTitle(String.valueOf(count));
        } else {
            binding.countText.setText("");
            binding.waveLoadingView.setCenterTitle("");
        }

    }

    public void rangeProgressDate(int maxCount) {
        this.maxCount = Math.abs(maxCount);
        if (loveDayModel != null && maxCount > 0) {
            int toDayCount = DateUtils.getCountDay(loveDayModel.isStartDay0(),
                    loveDayModel.getStartTime(),
                    DateUtils.toDay());
            int from = binding.progressBar.getProgress();
            int to = toDayCount * 100 / maxCount;
            onRangeProgress(from, to);
        }
    }

    private float rangeTo = -1f;

    private void onRangeProgress(float from, float to) {
        binding.waveLoadingView.setProgressValue((int) to);
    }

    public void refreshAnimation() {
        if (rangeTo >= 0) {
            onRangeProgress(0, rangeTo);
        }
    }

    public void setLayoutTitle(String topTitle, String bottomTitle) {
        String topTitle1 = topTitle;
        String bottomTitle1 = bottomTitle;

        if (TextUtils.isEmpty(topTitle1))
            topTitle1 = getContext().getString(R.string.main_love_top_title_default);
        if (TextUtils.isEmpty(bottomTitle1))
            bottomTitle1 = getContext().getString(R.string.main_love_bottom_title_default);
        binding.topTitle.setText(topTitle1);
        binding.bottomTitle.setText(bottomTitle1);

        binding.waveLoadingView.setTopTitle(topTitle1);
        binding.waveLoadingView.setBottomTitle(bottomTitle1);
    }
}
