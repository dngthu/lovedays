package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.LogUtils;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.util.ArrayList;
import java.util.List;

public class EventModels {
    private List<EventModel> eventModels;

    public EventModels() {

    }

    public void removeEvent(final String whatId) {
        Iterators.removeIf(getEventModels().iterator(), new Predicate<EventModel>() {
            @Override
            public boolean apply(@NullableDecl EventModel input) {
                return input != null && TextUtils.equals(input.getId(), whatId);
            }
        });
    }


    public void addEventModel(@NonNull final EventModel eventModel) {

        int index = Iterators.indexOf(getEventModels().iterator(), new Predicate<EventModel>() {
            @Override
            public boolean apply(@NullableDecl EventModel input) {
                return input != null && TextUtils.equals(input.getId(), eventModel.getId());
            }
        });
        if (index >= 0) {
            //for edit
            getEventModels().set(index, eventModel);
        } else {
            //for create new
            getEventModels().add(eventModel);
        }
    }


    @NonNull
    public List<EventModel> getEventAnnuals(boolean annual) {
        List<EventModel> result = new ArrayList<>();
        for (EventModel eventModel : getEventModels()) {
            if (annual  && eventModel.isAnnual()) {
                result.add(eventModel);
            }else if (!annual && !eventModel.isAnnual()){
                result.add(eventModel);
            }
        }
        return result;
    }

    @NonNull
    private List<EventModel> getEventModels() {
        if (eventModels == null) eventModels = new ArrayList<>();
        return eventModels;
    }


    public EventModel getEventModel(final String whatId) {
        List<EventModel> models = getEventModels();
        int index = Iterators.indexOf(models.iterator(), new Predicate<EventModel>() {
            @Override
            public boolean apply(@NullableDecl EventModel input) {
                return input != null && TextUtils.equals(input.getId(), whatId);
            }
        });
        if (index >= 0) return models.get(index);
        return null;
    }
}
