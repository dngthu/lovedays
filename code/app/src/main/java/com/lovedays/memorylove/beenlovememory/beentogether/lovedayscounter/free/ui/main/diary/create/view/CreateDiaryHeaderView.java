package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.CreateDiaryHeaderViewBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;

public class CreateDiaryHeaderView extends FrameLayout {
    public CreateDiaryHeaderView(@NonNull Context context) {
        super(context);
        initView();
    }

    public CreateDiaryHeaderView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CreateDiaryHeaderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private CreateDiaryHeaderViewBinding binding;

    private CreateDiaryListener createDiaryListener;

    private DiaryTable editDiary ;

    private String currentCover;

    public void setCreateDiaryListener(CreateDiaryListener createDiaryListener) {
        this.createDiaryListener = createDiaryListener;
    }

    private void initView() {
        binding = CreateDiaryHeaderViewBinding.inflate(LayoutInflater.from(getContext()));
        addView(binding.getRoot(), LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        bindView();
        bindAction();
    }

    private void bindView() {
        binding.dateTv.setText(DateUtils.getDateString(System.currentTimeMillis()));
    }

    private void bindAction() {
        binding.btnAddCover.setOnClickListener(v -> {
            if (createDiaryListener!=null){
                createDiaryListener.onLoadCover();
            }
        });

        binding.btnRemoveCover.setOnClickListener(v -> setCoverUrl(null));
    }


    public void setModel(DiaryTable diaryTable) {
        this.editDiary = diaryTable;
        setCoverUrl(diaryTable.getCover());
        updateLayout();
    }

    public void setCoverUrl(String cover){
        this.currentCover = cover;
        updateCover();
    }

    private void updateLayout() {
        if (editDiary!=null){
            String title = editDiary.getTitle();
            if (title!=null){
                binding.titleEdt.setText(editDiary.getTitle());
                binding.titleEdt.setSelection(title.length());
            }
        }
    }

    private void updateCover() {
        binding.btnRemoveCover.setVisibility((currentCover!=null && !currentCover.isEmpty()) ? VISIBLE : GONE);
        Glide.with(getContext())
                .load(currentCover)
                .apply(new RequestOptions().placeholder(R.drawable.bg_cover_diary_item))
                .apply(new RequestOptions().error(R.drawable.bg_cover_diary_item))
                .into(binding.coverV);
    }

    public DiaryTable getModel() {
        Editable text = binding.titleEdt.getText();
        String title = text != null ? text.toString().trim() : "";
        if (!title.isEmpty()) {
            if (editDiary!=null){
                editDiary.setTitle(title);
                editDiary.setCover(currentCover);
                editDiary.setTimeUpdate(System.currentTimeMillis());
                return editDiary;
            }
            return new DiaryTable(title, currentCover);
        }
        return null;
    }
}
