package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class TimeModel implements Parcelable {

    private long time;
    private String title;
    private String date;
    private int count;
    private boolean isComming;
    private String whatId;

    public TimeModel(long time, String title, String date, int count,String whatId) {
        this.time = time;
        this.title = title;
        this.date = date;
        this.count = count;
        this.whatId = whatId;
    }

    public TimeModel(long time, String title, String date, int count) {
        this.time = time;
        this.title = title;
        this.date = date;
        this.count = count;
    }

    public boolean isCustomEvent(){
        return !TextUtils.isEmpty(whatId);
    }

    public long getTime() {
        return time;
    }

    public String getWhatId() {
        return whatId;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public int getCount() {
        return count;
    }

    public boolean isComming() {
        return isComming;
    }

    public void setComming(boolean comming) {
        isComming = comming;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.time);
        dest.writeString(this.title);
        dest.writeString(this.date);
        dest.writeInt(this.count);
        dest.writeByte(this.isComming ? (byte) 1 : (byte) 0);
        dest.writeString(this.whatId);
    }

    protected TimeModel(Parcel in) {
        this.time = in.readLong();
        this.title = in.readString();
        this.date = in.readString();
        this.count = in.readInt();
        this.isComming = in.readByte() != 0;
        this.whatId = in.readString();
    }

    public static final Creator<TimeModel> CREATOR = new Creator<TimeModel>() {
        @Override
        public TimeModel createFromParcel(Parcel source) {
            return new TimeModel(source);
        }

        @Override
        public TimeModel[] newArray(int size) {
            return new TimeModel[size];
        }
    };
}
