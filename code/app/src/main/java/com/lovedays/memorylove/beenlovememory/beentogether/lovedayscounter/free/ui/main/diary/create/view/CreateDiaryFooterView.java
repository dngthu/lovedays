package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.create.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.CreateDiaryFooterViewBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.CreateDiaryHeaderViewBinding;

public class CreateDiaryFooterView extends FrameLayout {
    public CreateDiaryFooterView(@NonNull Context context) {
        super(context);
        initView();
    }

    public CreateDiaryFooterView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CreateDiaryFooterView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private CreateDiaryFooterViewBinding binding;

    private void initView() {
        binding = CreateDiaryFooterViewBinding.inflate(LayoutInflater.from(getContext()),
                this, false);
        addView(binding.getRoot(), LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        bindActions();
    }

    private CreateDiaryListener listener;

    public void setListener(CreateDiaryListener listener) {
        this.listener = listener;
    }

    private void bindActions() {
        binding.btnAddImage.setOnClickListener(view -> {
            if (listener != null) listener.addImage();
        });
        binding.btnAddText.setOnClickListener(view -> {
            if (listener != null) listener.addText();
        });
    }
}
