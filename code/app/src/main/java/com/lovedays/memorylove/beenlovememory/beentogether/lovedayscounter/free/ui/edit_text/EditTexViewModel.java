package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.edit_text;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsViewModel;

public class EditTexViewModel extends AbsViewModel {
    public String getCurrentBackground() {
        return getManager().getCurrentBackground();
    }
}
