package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.start;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;

import com.bumptech.glide.Glide;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.base.AbsActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.databinding.StartAppActBinding;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.model.LoveDayModel;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.MainActivity;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.DateUtils;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.utils.NotificationUtils;

import java.util.Calendar;


public class StartAppAct extends AbsActivity<StartAppActBinding, StartAppViewModel> {

    @Override
    protected int getLayoutId() {
        return R.layout.start_app_act;
    }

    public static Intent getIntent(@NonNull Context context){
        Intent intent = new Intent(context, StartAppAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                Intent.FLAG_ACTIVITY_CLEAR_TOP|
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected Class<StartAppViewModel> getViewModelClass() {
        return StartAppViewModel.class;
    }


    private DatePickerDialog datePickerDialog;

    @Override
    protected void initView() {
        LoveDayModel loveDayModel = viewModel.getCurrentLoveModel();
        if (loveDayModel != null) {
            buildNotification(loveDayModel);
            return;
        }

        initDataView();
        bindAction();
        initHandleStartTime();
    }

    private void initHandleStartTime() {
        viewModel.startDate.observe(this, new Observer<Long>() {
            @Override
            public void onChanged(Long aLong) {
                if (aLong != null) {
                    String dateStr = DateUtils.getDateString(aLong);
                    binding.dateStartLoveTv.setText(dateStr);
                }
            }
        });
    }

    private void initDataView() {

        //load bg
        Glide.with(this)
                .load(viewModel.getCurrentBackground())
                .into(binding.imageBg);

        // load current date
        viewModel.startDate.setValue(DateUtils.toDay());
    }

    private void bindAction() {

        binding.todayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.initLoveModel(DateUtils.toDay());
                navigateMain();
            }
        });

        binding.confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Long time = viewModel.startDate.getValue();
                long longTime;
                if (time != null) {
                    longTime = time;
                } else {
                    longTime = DateUtils.toDay();
                }
                viewModel.initLoveModel(longTime);
                navigateMain();
            }
        });

        binding.dateStartLoveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDatePicker(Calendar.getInstance());
            }
        });
    }


    private void buildNotification(@NonNull LoveDayModel loveDayModel) {
        //todo build notification
        LoveDayApp.getInstance().refreshNotifiStatusBar();

        navigateMain();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    viewModel.startDate.setValue(DateUtils.formatDay(myCalendar));
                }
            };


    private void showDialogDatePicker(Calendar myCalendar) {
        if (datePickerDialog == null) {
            datePickerDialog = new DatePickerDialog(this,
                    datePickerListener,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            Calendar maxCalendar = Calendar.getInstance();
            Calendar minCalendar = Calendar.getInstance();
            minCalendar.add(Calendar.YEAR, -100);

            datePickerDialog.getDatePicker().setMaxDate(maxCalendar.getTimeInMillis());
            datePickerDialog.getDatePicker().setMinDate(minCalendar.getTimeInMillis());
        }
        datePickerDialog.show();
    }

    private void navigateMain() {
        startActivity(MainActivity.getIntent(this));
        finish();
    }
}
