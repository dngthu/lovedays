package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListUpdateCallback;
import androidx.recyclerview.widget.RecyclerView;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.R;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;

import java.util.ArrayList;
import java.util.List;

public class DiaryContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER_COUNT = 1 ;
    private List<DiaryContentTable> list = new ArrayList<>();

    @NonNull
    private DiaryContentListener diaryListener;

    public DiaryContentAdapter(@NonNull DiaryContentListener diaryListener) {
        this.diaryListener = diaryListener;

    }

    public void submitList(List<DiaryContentTable> newList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(
                new DiaryContentDiffCallback(newList, list));
        diffResult.dispatchUpdatesTo(new ListUpdateCallback() {
            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position + HEADER_COUNT, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position + HEADER_COUNT, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition + HEADER_COUNT, toPosition+ HEADER_COUNT);
            }

            @Override
            public void onChanged(int position, int count, @Nullable Object payload) {
                notifyItemRangeChanged(position + HEADER_COUNT, count, payload);
            }
        });
        list.clear();
        this.list.addAll(newList);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == R.layout.diary_content_header_item) {
            return DiaryContentHeaderViewHolder.create(parent, diaryListener);
        }
        return DiaryContentViewHolder.create(parent, diaryListener);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return R.layout.diary_content_header_item;
        return R.layout.diary_content_item;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DiaryContentViewHolder) {
            if (position > 0 && position <= list.size()) {
                ((DiaryContentViewHolder) holder).bind(list.get(position - HEADER_COUNT));
            }
        } else if (holder instanceof DiaryContentHeaderViewHolder) {
            ((DiaryContentHeaderViewHolder) holder).bind();
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + HEADER_COUNT;
    }
}
