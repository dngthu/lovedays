package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.ui.main.diary.detail.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;

import java.util.List;

public class DiaryContentDiffCallback extends DiffUtil.Callback  {

    List<DiaryContentTable> newList;
    List<DiaryContentTable> oldList;


    public DiaryContentDiffCallback(@NonNull List<DiaryContentTable> newList,
                                    @NonNull List<DiaryContentTable> list) {
        this.newList = newList;
        this.oldList = list;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        DiaryContentTable old = oldList.get(oldItemPosition);
        DiaryContentTable newM = newList.get(newItemPosition);
        return TextUtils.equals(old.getId(), newM.getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        DiaryContentTable old = oldList.get(oldItemPosition);
        DiaryContentTable newM = newList.get(newItemPosition);
        return TextUtils.equals(old.getId(), newM.getId())
                && old.getType()== newM.getType()
                && old.getTextStyle()== newM.getTextStyle()
                && old.isTextCenter()== newM.isTextCenter()
                && TextUtils.equals(old.getContent(), newM.getContent());
    }
}
