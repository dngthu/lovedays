package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.cache;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.LoveDayApp;

/**
 * Created by BTD on 5/9/2018.
 */

public class PreferencesImpl implements Preferences {
    private Gson mGSon;
    private static Preferences utilShareFrefence;
    private SharedPreferences sharedPreferences;
    private static final String NAME = "APP_NAME";

    public PreferencesImpl() {
        mGSon = new Gson();
        sharedPreferences = LoveDayApp.getInstance().getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public void registerChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void ungisterChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    @NonNull
    public static Preferences getInstance() {
        if (utilShareFrefence == null) {
            utilShareFrefence = new PreferencesImpl();
        }
        return utilShareFrefence;
    }

    public <T> T getData(String key, T def, Class<T> c) {
        try {
            if (c == String.class) {
                return (T) sharedPreferences.getString(key, (String) def);
            } else if (c == Integer.class) {
                return (T) Integer.valueOf(sharedPreferences.getInt(key, ((Integer) def).intValue()));
            } else if (c == Boolean.class) {
                return (T) Boolean.valueOf(sharedPreferences.getBoolean(key, ((Boolean) def).booleanValue()));
            } else if (c == Float.class) {
                return (T) Float.valueOf(sharedPreferences.getFloat(key, ((Float) def).floatValue()));
            } else if (c == Long.class) {
                return (T) Long.valueOf(sharedPreferences.getLong(key, ((Long) def).longValue()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public <T> T getObject(String key, Class<T> c) {
        return (T) mGSon.fromJson(sharedPreferences.getString(key, ""), c);

    }

    public <T> void putData(String key, T value) {
        if (value instanceof String) {
            sharedPreferences.edit().putString(key, String.valueOf(value)).commit();
        } else if (value instanceof Integer) {
            sharedPreferences.edit().putInt(key, (Integer) value).commit();
        } else if (value instanceof Boolean) {
            sharedPreferences.edit().putBoolean(key, (Boolean) value).commit();
        } else if (value instanceof Float) {
            sharedPreferences.edit().putFloat(key, (Float) value).commit();
        } else if (value instanceof Long) {
            sharedPreferences.edit().putLong(key, (Long) value).commit();
        } else {
            sharedPreferences.edit().putString(key, mGSon.toJson(value)).commit();
        }
    }

    @Override
    public void remove(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }

    @Override
    public <T> LiveData<T> getDataLive(String key, T def) {
        MutableLiveData<T> data = new MutableLiveData<>();
        data.postValue(getData(key, def));
        return data;
    }


    public <T> T getData(String key, T def) {
        if (def instanceof String) {
            return (T) sharedPreferences.getString(key, (String) def);
        } else if (def instanceof Integer) {
            return (T) Integer.valueOf(sharedPreferences.getInt(key, ((Integer) def).intValue()));
        } else if (def instanceof Boolean) {
            return (T) Boolean.valueOf(sharedPreferences.getBoolean(key, ((Boolean) def).booleanValue()));
        } else if (def instanceof Float) {
            return (T) Float.valueOf(sharedPreferences.getFloat(key, ((Float) def).floatValue()));
        } else if (def instanceof Long) {
            return (T) Long.valueOf(sharedPreferences.getLong(key, ((Long) def).longValue()));
        }
        return null;
    }
}
