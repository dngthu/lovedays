package com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.model;

import androidx.annotation.NonNull;
import androidx.room.Embedded;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryContentTable;
import com.lovedays.memorylove.beenlovememory.beentogether.lovedayscounter.free.diary.table.DiaryTable;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.util.List;

public class DiaryEntity {

    @NonNull
    @Embedded
    private DiaryTable diaryTable;

    private List<DiaryContentTable> diaryContents;

    public DiaryEntity(@NonNull DiaryTable diaryTable) {
        this.diaryTable = diaryTable;
    }

    public DiaryEntity(@NonNull DiaryTable diaryTable,
                       List<DiaryContentTable> diaryContents) {
        this.diaryTable = diaryTable;
        this.diaryContents = diaryContents;
    }

    @NonNull
    public DiaryTable getDiaryTable() {

        if (diaryContents!=null && !diaryContents.isEmpty()){
            int index = Iterators.indexOf(diaryContents.iterator(),
                    input -> input!=null
                    && input.getSubContent()!=null);

            if (index>=0){
                DiaryContentTable content = diaryContents.get(index);
                diaryTable.setDesciption(content.getSubContent());
            }else {
                diaryTable.setDesciption("");
            }
        }else {
            diaryTable.setDesciption("");
        }
        return diaryTable;
    }

    public void setDiaryTable(@NonNull DiaryTable diaryTable) {
        this.diaryTable = diaryTable;
    }

    public List<DiaryContentTable> getDiaryContents() {
        return diaryContents;
    }

    public void setDiaryContents(List<DiaryContentTable> diaryContents) {
        this.diaryContents = diaryContents;
    }
}
