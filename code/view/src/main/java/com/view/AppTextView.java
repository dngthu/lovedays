package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class AppTextView extends AppCompatTextView {
    public AppTextView(Context context) {
        super(context);
        initView();
    }

    public AppTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AppTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        Typeface typeface = FontCache.loadFont(Fonts.Roboto_Medium, getContext());
        setTypeface(typeface);
    }
}
