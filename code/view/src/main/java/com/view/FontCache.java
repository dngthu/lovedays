package com.view;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

public class FontCache {

    private static HashMap<String, Typeface> fontCache = new HashMap<>();
    private static HashMap<Fonts, Typeface> fontCacheType = new HashMap<>();

    private static Typeface getTypeface(Context context, String fontname) {
        Typeface typeface = fontCache.get(fontname);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontname);
            } catch (Exception e) {
                return null;
            }
            fontCache.put(fontname, typeface);
        }
        return typeface;
    }

    public static Typeface loadFont(Fonts fonts, Context context) {
        Typeface typeface = fontCacheType.get(fonts);

        if (typeface == null) {
            try {
                String fontName;
                switch (fonts) {
                    case BaseFiolexGirl:
                        fontName = "BaseFiolexGirl.ttf";
                        break;
                    case Roboto_Medium:
                        fontName ="Roboto-Medium.ttf";
                        break;
                    default:
                        fontName = "BaseFiolexGirl.ttf";
                        break;
                }
                typeface = getTypeface(context, fontName);
            } catch (Exception e) {
                return null;
            }
            fontCacheType.put(fonts, typeface);
            return typeface;
        }
        return typeface;
    }
}