package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class AppButton extends androidx.appcompat.widget.AppCompatButton {
    public AppButton(Context context) {
        super(context);
        initView();
    }

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AppButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setAllCaps(false);
        Typeface typeface = FontCache.loadFont(Fonts.Roboto_Medium, getContext());
        setTypeface(typeface);
    }
}
