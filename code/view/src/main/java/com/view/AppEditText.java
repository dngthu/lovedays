package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class AppEditText extends AppCompatEditText {
    public AppEditText(Context context) {
        super(context);
        initView();
    }

    public AppEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AppEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        Typeface typeface = FontCache.loadFont(Fonts.Roboto_Medium, getContext());
        setTypeface(typeface);
    }
}
