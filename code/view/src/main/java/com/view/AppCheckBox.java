package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class AppCheckBox extends androidx.appcompat.widget.AppCompatCheckBox {
    public AppCheckBox(Context context) {
        super(context);
        initView();
    }

    public AppCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AppCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView(){
        Typeface typeface = FontCache.loadFont(Fonts.Roboto_Medium, getContext());
        setTypeface(typeface);
    }
}
