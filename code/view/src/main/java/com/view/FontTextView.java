package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class FontTextView extends TextView {
    public FontTextView(Context context) {
        super(context);
        initView();
    }

    public FontTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public FontTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }
    private void initView(){
        Typeface typeface = FontCache.loadFont(Fonts.BaseFiolexGirl, getContext());
        setTypeface(typeface);
    }
}
